# CrashSafe

[CrashSafe.me](https://crashsafe.me)

Git SHA: b18e3194ae72961ddd8621d6af295a51f70d9247

## Members

| Name           | EID     | GitLab ID    | Phase 1   | Phase 2    | Phase 3    | Phase 4   |
| -------------- | ------- | ------------ | --------- | ---------- | ---------- | --------- |
| Alisha Zute    | abz266  | alishazute   | 12 -> 11  | 30 -> 24  | 30 -> 12  | 15 -> 7   |
| Branden James  | btj399  | btj3991      | 12 -> 9   | 30 -> 30* | 30 -> 20  | 15 -> 13  |
| Grant Fischer  | gef432  | grantfischer | 12 -> 14* | 30 -> 30  | 30 -> 20  | 15 -> 12  |
| Greg Pauloski  | jgp992  | gpauloski    | 12 -> 13  | 30 -> 25  | 30 -> 20* | 15 -> 12  |
| Kirill Belous  | kb39398 | kbelous      | 12 -> 15  | 30 -> 30  | 30 -> 15  | 15 -> 5   |
| Kiarash Sadr   | ss65728 | dsynkd       | 12 -> 10  | 30 -> 25  | 30 -> 20  | 15 -> 10  |

- Phase completion times are reported as (expected hours) -> (actual hours).
- \* denotes the leader for that phase.

## Resources

* [Project Page](https://www.cs.utexas.edu/users/downing/cs373/projects/IDB.html)
* [Useful Dev Resources](https://www.cs.utexas.edu/users/downing/cs373/Resources.html)

## Usage

### Starting Docker Images for Local Development

To start the docker images in the background:
* Build Docker images: `$ make docker-build-{frontend,backend}`
* Start Docker images: `$ make docker-{frontend,backend}`
* Stop Docker images: `$ make docker-stop-{frontend,backend}`

The webserver will be started on `localhost:3000` and the API server will be started on `localhost:5000`.

Alternatively, you can start an interactive docker shell with `$ make docker-{frontend,backend}-interactive` then
use `make npm` and `make flask` to start the front and backend servers respectively.

Note: to connect to the database for local development, the following environment variables must be defined:
```
DB_NAME=<database name>
DB_HOST=<database host>
DB_PORT=<database host port>
DB_USER=<database username>
DB_PASSWORD=<database password>
```
These environment variables are already defined on the EC2 server and for the GitLab pipeline. HOWEVER, they will not
be defined in the Docker image on the EC2 server. Many scripts, for example `backend/db_utils.py` will also read these
values from `backend/.env` if it exists.

### Deploy on EC2 Server

Deploy the front and backend with `$ sudo make deploy`. Stop with `$ sudo make stop-deploy`. Use `$ docker ps` to see the running images. The front and back end can also be individually stopped/started following the instructions above in "Starting Docker Images for Local Development".

### Develop on a New Branch

Git branches allow for us to isolate our work from each other until we have completed
whatever task we are currently working on (e.g. a feature, bug fix, refactor). It also
make tracking changes to the repository easier as all commits that are a part of one
large feature addition will be merge at the same time instead of with incremental commits
to master.

1. Create a new branch by either going to the GitLab repo -> Repository -> Branches -> New Branch or
   from the command line by running:
   ```
   $ git pull  # make sure you are up to date
   $ git checkout -b {name_of_new_branch}
   $ git push origin {name_of_new_branch}  # publish new branch on GitLab
   ```
2. Now you can make changes and `git add`, `git commit`, and `git push` to your new branch.
   Note `git push` will use the current branch unless you specifiy a branch with 
   `git push origin/branch_name`. 
3. To make a merge request, go to the GitLab repo -> Repository -> Branches and click
   merge request on your branch. This should be done once you have finished your current task
   and all associated documentation or testing.
