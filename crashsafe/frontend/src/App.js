import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { matchPath } from "react-router";
import 'bootstrap/dist/css/bootstrap.min.css';
// import { render } from '@testing-library/react';
// import './App.css';

import Home from './components/pages/Home.js';
import SearchResults from './components/pages/SearchResults.js';
import About from './components/pages/About.js';
import CarBrand from './components/pages/CarBrand.js';
import CarModel from './components/pages/CarModel.js';
import CrashTest from './components/pages/CrashTest.js';
import Provider from './components/pages/Provider.js';
import NotFoundPage from './components/pages/NotFoundPage.js'
import Visualizations from './components/pages/Visualizations.js'

import Header from './components/Header.js';

function App() {
  return (
    <React.Fragment>
      <Header />
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/search/:query" component={SearchResults}/>
          <Route path="/about" component={About} />
          <Route path="/carbrand" component={CarBrand} />
          <Route path="/carmodel" component={CarModel} />
          <Route path="/crashtest" component={CrashTest} />
          <Route path="/provider" component={Provider} />
          <Route path="/visualizations" component={Visualizations} />
          <Route path="/*" component={NotFoundPage} />
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
