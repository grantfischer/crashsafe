import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { shape } from 'prop-types';
import {act} from "react-dom/test-utils"
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect, should} from 'chai';

import App from './App';
import Header from './components/Header'
import Home from './components/pages/Home'
import CarBrand from './components/pages/CarBrand'
import CarBrandInst from './components/pages/CarBrandInst'
import CarModel from './components/pages/CarModel'
import CarModelInst from './components/pages/CarModelInst'
import CrashTest from './components/pages/CrashTest'
import CrashTestInst from './components/pages/CrashTestInst'
import Provider from './components/pages/Provider'
import ProviderInst from './components/pages/ProviderInst'
import About from './components/pages/About'

// Test every page on the website works
// For model instance pages, we test loading a null page, i.e. don't load data from the database just
// render the html elements

// Tests based on:
// https://medium.com/@houstoncbreedlove/basics-intro-to-testing-react-components-with-mocha-chai-enzyme-and-sinon-c8b82ce58df8
// https://stackoverflow.com/questions/48895514/how-do-you-test-router-match-params-with-jest-and-enzyme
// https://stackoverflow.com/questions/52141775/node-express-mocha-test-typeerror-chai-request-is-not-a-function/52145278

var chai = require('chai');
var chaiHttp = require('chai-http')
chai.use(chaiHttp);

// Instantiate router context
const router = route => ({
  history: new BrowserRouter().history,
  route,
});

const createContext = route => ({
  context: { ...router(route) },
  childContextTypes: { router: shape({}) },
});

export function mountWrap(node, route) {
  return mount(node, createContext(route));
}

configure({ adapter: new Adapter() });

describe('App', function() {
  it('renders page', function() {
    const wrapper = shallow(<App />); 
    expect(wrapper).to.exist
  });
});

describe('Home', function() {
  it('renders page', function() {
    const wrapper = shallow(<Home />); 
    expect(wrapper).to.exist
  });
});

describe('Home', function() {
  it('renders page', function() {
    const wrapper = shallow(<Home />); 
    expect(wrapper).to.exist
  });
});

describe('Header', function() {
  it('renders page', function() {
    const wrapper = shallow(<Header />); 
    expect(wrapper).to.exist
  });
});

describe('CarBrand', function() {
  it('renders page', function() {
    const wrapper = shallow(<CarBrand />); 
    expect(wrapper).to.exist
  });
});

describe('CarModel', function() {
  it('renders page', function() {
    const wrapper = shallow(<CarModel />); 
    expect(wrapper).to.exist
  });
});

describe('CrashTest', function() {
  it('renders page', function() {
    const wrapper = shallow(<CrashTest />); 
    expect(wrapper).to.exist
  });
});

describe('Provider', function() {
  it('renders page', function() {
    const wrapper = shallow(<Provider />); 
    expect(wrapper).to.exist
  });
});

describe('About', function() {
  it('renders page', function() {
    const wrapper = shallow(<About />); 
    expect(wrapper).to.exist
  });
});

describe('CarBrandInst', () => {
  let props;
  let component;

  const div = document.createElement('div');
  const wrappedMount = () => mountWrap(<CarBrandInst {...props} />, div);

  beforeEach(() => {
    props = {
      id: null,
    };
    if (component) component.componentWillUnmount();
  });

  it("renders page", () => {
    chai.request('http://localhost:3000').get('/carbrand/null')
        .end(function(err, res){
          expect(res == undefined).is.true
        });
  });

});

describe('CarModelInst', () => {
  let props;
  let component;

  const div = document.createElement('div');
  const wrappedMount = () => mountWrap(<CarModelInst {...props} />, div);

  beforeEach(() => {
    props = {
      id: null,
    };
    if (component) component.unmount();
  });

  it("renders page", () => {
    chai.request('http://localhost:3000').get('/carmodel/null')
        .end(function(err, res){
          expect(res == undefined).is.true
        });
  });

});

describe('CrashTestInst', () => {
  let props;
  let component;

  const div = document.createElement('div');
  const wrappedMount = () => mountWrap(<CrashTestInst {...props} />, div);

  beforeEach(() => {
    props = {
      id: null,
    };
    if (component) component.unmount();
  });

  it("renders page", () => {
    chai.request('http://localhost:3000').get('/crashtest/null')
        .end(function(err, res){
          expect(res == undefined).is.true
        });
  });

});

describe('ProviderInst', () => {
  let props;
  let component;

  const div = document.createElement('div');
  const wrappedMount = () => mountWrap(<ProviderInst {...props} />, div);

  beforeEach(() => {
    props = {
      id: null,
    };
    if (component) component.unmount();
  });

  it("renders page", () => {
    chai.request('http://localhost:3000').get('/provider/null')
        .end(function(err, res){
          expect(res == undefined).is.true
        });
  });

});