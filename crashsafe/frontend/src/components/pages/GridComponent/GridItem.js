import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Highlighter from "react-highlight-words";
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  Spinner,
} from 'reactstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Rating } from '@material-ui/lab';

import { FaTwitter, FaPlus } from 'react-icons/fa';


class GridItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tableType: props.tableType,
            entryData: props.entryData,
            query: props.query,
            addFunction: props.addFunction,
        }
    }

    componentDidMount() {
    }

    render(){
        const brand = Object(this.state.entryData);
        return(
            <Col>
                {this.state.tableType === "brands" && <div style={{width: "250px", height: "410px", margin:"4px"}}>
                    <Card style={{height: "100%"}}>
                        <div style={{textAlign: "right"}}>
                            <Button onClick={() => {this.state.addFunction(this.state.entryData)}} style={{color: "blue"}}>
                                <FaPlus />
                            </Button>
                        </div>
                        <Link to={location => `${location.pathname}/${this.state.entryData.id}`}>
                            <CardActionArea style={{textAlign: "center"}}>
                                <img
                                src={brand.logo}
                                style={{width: "40%", height: "100%"}}
                                />
                                <CardContent>
                                <Typography gutterBottom variant="h4" component="h2">
                                    <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.name}
                                    />
                                </Typography>
                                <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                    <br/>
                                    {brand.year_founded && "Est. "}
                                    {brand.year_founded && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.year_founded.toString()}
                                    />}
                                    {brand.year_founded && <br/>}
                                    {'Number of Models: '}
                                    {brand.model_count && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.model_count.toString()}
                                    />}
                                    {brand.model_count && <br/>}
                                    <br/>
                                    {brand.user_rating && brand.user_rating !== 0 && "Avgerage User Rating: "}
                                    {brand.user_rating && brand.user_rating !== 0 && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.user_rating.toString() + "%"}
                                    />}
                                    {brand.user_rating && <br/>}
                                    {brand.safety_rating && brand.safety_rating !== 0 && "Avgerage Safety Rating: "}
                                    {brand.safety_rating && brand.safety_rating !== 0 && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.safety_rating.toString()}
                                    />}
                                    {brand.safety_rating && brand.safety_rating !== 0 && <Rating name="half-rating-read" defaultValue={brand.safety_rating} precision={0.01} readOnly />}
                                    {brand.safety_rating && <br/>}
                                    
                                    
                                    
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                        <CardActions style={{position: "absolute", bottom: 0, right: 45}}>
                            {brand.website ? <Button color="primary" size="large" href={brand.website}>Website</Button> : ""}
                            {brand.twitter_handle ? <Button color="primary" size="large" href={"https://twitter.com/" + brand.twitter_handle}><FaTwitter /></Button> : ""}
                        </CardActions>
                    </Card>
                </div>}

                {this.state.tableType === "providers" && <div style={{width: "300px", height: "440px", margin:"4px"}}>
                    <Card style={{height: "100%"}}>
                        <div style={{textAlign: "right"}}>
                            <Button onClick={() => {this.state.addFunction(this.state.entryData)}} style={{color: "blue"}}>
                                <FaPlus />
                            </Button>
                        </div>
                        <Link to={location => `${location.pathname}/${this.state.entryData.id}`}>
                            <CardActionArea style={{textAlign: "center"}}>
                                <img
                                src={brand.logo}
                                style={{width: "60%", height: "100%"}}
                                />
                                <CardContent>
                                <Typography gutterBottom variant="h6" component="h2">
                                    <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.name}
                                    />
                                </Typography>
                                <Typography gutterBottom variant="body2" component="p">
                                    <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.location}
                                    />
                                </Typography>
                                <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                    <br/>
                                    {brand.year_founded && "Est. "}
                                    {brand.year_founded && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.year_founded.toString()}
                                    />}
                                    {brand.year_founded && <br/>}

                                    {brand.location_count && "Number of Locations: "}
                                    {brand.location_count && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.location_count.toString()}
                                    />}
                                    {brand.location_count && <br/>}
                                    {brand.employee_count && "Number of Employees: "}
                                    {brand.employee_count && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.employee_count.toString()}
                                    />}
                                    {brand.employee_count && <br/>}
                                    {brand.test_count && "Number of Crash Tests: "}
                                    {brand.test_count && <Highlighter
                                        highlightClassName="YourHighlightClass"
                                        searchWords={[this.state.query]}
                                        autoEscape={true}
                                        textToHighlight={brand.test_count.toString()}
                                    />}
                                    {brand.test_count && <br/>}
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                        <CardActions style={{position: "absolute", bottom: 0, right: 70}}>
                            {brand.website ? <Button color="primary" size="large"l href={brand.website}>Website</Button> : ""}
                            {brand.twitter_handle ? <Button color="primary" size="large" href={"https://twitter.com/" + brand.twitter_handle}><FaTwitter /></Button> : ""}
                        </CardActions>
                    </Card>
                </div>}
            </Col>
        );
    }
}

export default GridItem;