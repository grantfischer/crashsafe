import React, { useState } from 'react';
import {
    Row, Col, Dropdown, DropdownMenu, DropdownToggle, DropdownItem,
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
  } from 'reactstrap';
  import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';
import TextField from '@material-ui/core/TextField';
import TablePagination from '@material-ui/core/TablePagination';
import GridItem from './GridItem';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import SwitchUI from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';

// import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import { FaSearch } from 'react-icons/fa';

import CompareComponent from './../../CompareComponent/CompareComponent'
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


const useStyles = makeStyles((theme) => ({
  root: {
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
class GridComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // Table Info
            originalData: [...props.tableData],
            table: props.table,
            num: props.num,

            // Paging Info
            page: 1,
            pageSize: 5,

            // Search Info
            query: "",
            items: [...props.tableData],

            // Sorting Info
            brandDataAccessors: ["name", "year_founded", "model_count", "user_rating", "safety_rating"],
            brandDataNames: ["Name", "Year Founded", "Model Count", "User Rating", "Safety Rating"],
            brandSortBy: 0,

            testerDataAccessors: ["name", "year_founded", "location", "location_count", "employee_count", "test_count"],
            testerDataNames: ["Name", "Year Founded", "Location", "Location Count", "No. Employees", "Test Count"],
            testerSortBy: 0,
            
            anchorEl: null,
            descending: false,

            // Filtering Info
            brandFilters: ["", "", "", "", ""],
            testerFilters: ["", "", "", "", "", ""],

            // Comparing Info
            open: false,    
            maxWidth: 'sm',
            compareData: [],
            openError: false,
            comparable: false,
        }
    }

    // Paging

    handleChangeRowsPerPage = event => {
        this.setState({pageSize: Number(event.target.value)});
    }

    handlePageChange = (event, page) => {
        this.setState({page: page});
    }

    // Searching ---------------------------------------------------------------------

    searchIncludesItem = (item) => {
        switch(this.state.table) {
            case "brands":
                let bName = item.name ? item.name : "";
                let hasbName = bName.toLowerCase().includes(this.state.query.toLowerCase());
                let bYear = item.year_founded ? item.year_founded : "";
                let hasbYear = bYear.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let bNum = item.model_count;
                let hasbNum = bNum.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let bUser = item.user_rating ? item.user_rating : ""; 
                let hasbUser = bUser.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let bSafety = item.safety_rating ? item.safety_rating : "";
                let hasbSafety = bSafety.toString().toLowerCase().includes(this.state.query.toLowerCase());
                return hasbName || hasbYear || hasbNum || hasbUser || hasbSafety;
            case "providers":
                let pName = item.name ? item.name : "";
                let haspName = pName.toLowerCase().includes(this.state.query.toLowerCase());
                let pYear = item.year_founded ? item.year_founded : "";
                let haspYear = pYear.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let pLoc = item.location ? item.location : "";
                let haspLoc = pLoc.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let pLocations = item.location_count ? item.location_count : "";
                let haspLocations = pLocations.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let pEmployees = item.employee_count ? item.employee_count : ""; 
                let haspEmployees = pEmployees.toString().toLowerCase().includes(this.state.query.toLowerCase());
                let pTests = item.test_count;
                let haspTests = pTests.toString().toLowerCase().includes(this.state.query.toLowerCase());
                return haspName || haspYear || haspLoc || haspLocations || haspEmployees || haspTests;
        }
    }

    handleSearch = (event, index, value) => {
        this.setState({query: event.target.value});
    }

    handleSearchSubmit = () => {
        let refinedItems = [];
        this.state.items.map((item, idx) => {
            if (this.searchIncludesItem(item))
                refinedItems.push(item)
        });
        this.setState({page: 1, reRender: !this.state.reRender, items: refinedItems});
    }

    // Sorting ---------------------------------------------------------------------
    
    handleClick = (event) => {
        this.setState({anchorEl: event.currentTarget})
    }
    
    handleClose = (event) => {
        switch(this.state.table) {
            case "brands":
                this.setState({anchorEl: null, brandSortBy: (event.target.value ? event.target.value : this.state.brandSortBy)})
                break;
            case "providers":
                this.setState({anchorEl: null, testerSortBy: (event.target.value ? event.target.value : this.state.testerSortBy)})
                break;
        }
    }

    //Comparer Function [Credit: https://www.c-sharpcorner.com/UploadFile/fc34aa/sort-json-object-array-based-on-a-key-attribute-in-javascrip/]
    getSortOrder(prop) {  
        switch(this.state.descending) {
            case true:
                return function(b, a) {  
                    if (a[prop] > b[prop]) {  
                        return 1;  
                    } else if (a[prop] < b[prop]) {  
                        return -1;  
                    }  
                    return 0;  
                }  
            case false:
                return function(a, b) {  
                    if (a[prop] > b[prop]) {  
                        return 1;  
                    } else if (a[prop] < b[prop]) {  
                        return -1;  
                    }  
                    return 0;  
                }  
        }
    }

    handleSort = () => {
        let criteria = "";
        let sortedItems = [];
        switch(this.state.table) {
            case "brands":
                criteria = this.state.brandDataAccessors[this.state.brandSortBy];
                break;
            case "providers":
                criteria = this.state.testerDataAccessors[this.state.testerSortBy];
                break;
        }
        this.state.items.map((item, idx) => {
            sortedItems.push(item);
        });
        sortedItems.sort(this.getSortOrder(criteria))
        this.setState({reRender: !this.state.reRender, items: sortedItems});
    }  

    handleSortMode = () => {
        this.setState({descending: !this.state.descending});
    }

    // Filtering ---------------------------------------------------------------------
    
    handleFilter = (event, index, value) => {
        switch(this.state.table) {
            case "brands":
                this.state.brandFilters[event.target.id] = event.target.value;
                this.setState({brandFilters: this.state.brandFilters});
                break;
            case "providers":
                this.state.testerFilters[event.target.id] = event.target.value;
                this.setState({testerFilters: this.state.testerFilters});
                break;
        }
    }

    filterIncludesItem(filterNum, item) {
        switch(this.state.table) {
            case "brands":
                let bVal = item[this.state.brandDataAccessors[filterNum]] ? item[this.state.brandDataAccessors[filterNum]] : "";
                return this.state.brandFilters[filterNum].length === 0 || bVal.toString().toLowerCase().includes(this.state.brandFilters[filterNum].toLowerCase());
            case "providers":
                let pVal = item[this.state.testerDataAccessors[filterNum]] ? item[this.state.testerDataAccessors[filterNum]] : "";
                return this.state.testerFilters[filterNum].length === 0 || pVal.toString().toLowerCase().includes(this.state.testerFilters[filterNum].toLowerCase());
        }
    }

    handleFilterSubmit = (event, index, value) => {
        let filterNum = event.target.id;
        let refinedItems = [];

        this.state.items.map((item, idx) => {
            let containsFilter = [];
            switch(this.state.table) {
                case "brands":
                    this.state.brandDataAccessors.map((val, idx) => {
                        containsFilter.push(this.filterIncludesItem(idx, item))
                    });
                    break;
                case "providers":
                    this.state.testerDataAccessors.map((val, idx) => {
                        containsFilter.push(this.filterIncludesItem(idx, item))
                    });
                    break;
            }
            let include = containsFilter.reduce((nextVal, currVal) => nextVal && currVal)

            if (include)
                refinedItems.push(item)
        });
        this.setState({page: 1, reRender: !this.state.reRender, items: [...refinedItems]});
    }

    handleResetSort = () => {
        this.setState({
            brandFilters: ["", "", "", "", ""],
            testerFilters: ["", "", "", "", "", ""],
            query: "",
            items: [...this.state.originalData],
            openError: false,
            reRender: !this.state.reRender
        })
    }

    handleResetCompare = () => {
    	this.setState({
    		compareData: [],
    		comparable: false
    	})
    }

    // Comparing
    toggleCompare = () => {
        this.setState({
            open: !this.state.open ,
        })
    }

    handleAddCompare = (item) => {
        let newSize = "";
        let comp = false;
        switch(this.state.compareData.length) {
            case 0:
                newSize = 'sm';
                comp = true;
                break;
            case 1:
                newSize = 'md';
                comp = true;
                break;
            case 2:
                newSize = 'lg';
                comp = true;
                break;
            case 3:
                newSize = 'xl';
                comp = true;
                break;
            default:
                this.setState({
                    openError: true,
                });
                break;
        }

        if (this.state.compareData.length <= 3) {
            this.setState(state => {
                const compareData = state.compareData.concat(item);
                return {
                    compareData,
                    maxWidth: newSize,
                    comparable: comp,
                };
            });
        }
    }

    render() {
        // console.log(this.state);
        return (
            <div style={{margin: "40px"}}>
                {/* ----------------------------------- Overlays ----------------------------------- */}
                <Dialog onClose={this.toggleCompare} open={this.state.open} maxWidth={this.state.maxWidth}>
                    {this.state.table === "brands" && <DialogTitle>Compare Car Brands</DialogTitle>}
                    {this.state.table === "providers" && <DialogTitle>Compare Crash Test Providers</DialogTitle>}
                    <div style={{overflow: "scroll"}}>
                        <CompareComponent table={this.state.table} data={this.state.compareData}/>
                    </div>
                </Dialog>
                <Snackbar
                open={this.state.openError} 
                autoHideDuration={5000} 
                onClose={() => this.setState({openError: false})}>
                    <MuiAlert elevation={6} variant="filled" severity="error">
                        Can't Compare More Than 4 Items
                    </MuiAlert>
                </Snackbar>
                {/* ----------------------------------- Header ----------------------------------- */}
                <Row>
                    <Col md="2">
                        <Button style={{margin: "5px"}} color={"primary"} onClick={this.handleClick}>
                            Sort By
                        </Button>
                        <Button style={{margin: "5px"}} color={"success"} onClick={this.handleSort}>
                            Sort
                        </Button>
                        <Button style={{margin: "5px"}} color={"danger"} onClick={this.handleResetSort}>
                            Reset
                        </Button>
                        <Menu
                        id="simple-menu"
                        anchorEl={this.state.anchorEl}
                        keepMounted
                        open={Boolean(this.state.anchorEl)}
                        onClose={this.handleClose}
                        style={{textAlign: "center"}}
                        >
                            {this.state.table === "brands" && this.state.brandDataNames.map((value, idx) => {
                                return <MenuItem onClick={this.handleClose} value={idx}>{value}</MenuItem>
                            })}
                            {this.state.table === "providers" && this.state.testerDataNames.map((value, idx) => {
                                return <MenuItem onClick={this.handleClose} value={idx}>{value}</MenuItem>
                            })}
                            <FormControlLabel
                                control={<SwitchUI size="small" checked={this.state.descending} onChange={this.handleSortMode} />}
                                label="Reverse"
                            />
                        </Menu>
                    </Col>
                    <Col md="8">
                            {this.state.table === "brands" && this.state.brandDataNames.map((value, idx) => {
                                return <React.Fragment style={{margin: "20px", leftMargin: "10px", rightMargin: "20px", height: "85%"}}>
                                    <TextField id={idx} style={{marginTop: "10px", leftMargin: "20px", height: "50px", width: "130px"}} label={value} value={this.state.brandFilters[idx]} variant="outlined" onChange={this.handleFilter}/>
                                    <Button id={idx} style={{marginTop: "10px", marginLeft: "5px", marginRight: "10px", height: "50px"}} color={"primary"} onClick={this.handleFilterSubmit}><FaSearch/></Button>
                                </React.Fragment>
                            })}
                            {this.state.table === "providers" && this.state.testerDataNames.map((value, idx) => {
                                return <React.Fragment style={{margin: "20px", leftMargin: "10px", rightMargin: "20px", height: "85%"}}>
                                    <TextField id={idx} style={{marginTop: "10px", leftMargin: "20px", height: "50px", width: "130px"}} label={value} value={this.state.testerFilters[idx]} variant="outlined" onChange={this.handleFilter}/>
                                    <Button id={idx} style={{marginTop: "10px", marginLeft: "5px", marginRight: "10px", height: "50px"}} color={"primary"} onClick={this.handleFilterSubmit}><FaSearch/></Button>
                                </React.Fragment>
                            })}
                    </Col>
                    <Col md="2">
                        <Button disabled={!this.state.comparable} style={{margin: "10px"}} color={"primary"} size={"md"} onClick={this.toggleCompare}>
                        	Compare ({this.state.compareData.length})
                        </Button>
                        <Button style={{margin: "5px"}} color={"danger"} onClick={this.handleResetCompare}>
                        	Reset
                        </Button>
                    </Col>  
                </Row>

                {/* ----------------------------------- Body ----------------------------------- */}
                {!this.state.reRender && <Row style={{margin: "40px"}}>
                    {this.state.items.length !== 0 && this.state.items.map((tableData, i) => {
                        if (i >= (this.state.page - 1) * this.state.pageSize && i < this.state.page * this.state.pageSize) { // Item is within correct page
                            return <div style={{marginBottom: "20px"}}>
                                <GridItem addFunction={(itemData) => {this.handleAddCompare(itemData)}} query={this.state.query} tableType={this.state.table} entryData={tableData}/>
                            </div>
                        }
                    })}
                </Row>}
                {this.state.reRender && <Row style={{margin: "40px"}}>
                    {this.state.items.length !== 0 && this.state.items.map((tableData, i) => {
                        if (i >= (this.state.page - 1) * this.state.pageSize && i < this.state.page * this.state.pageSize) { // Item is within correct page
                            return <div style={{marginBottom: "20px"}}>
                                <GridItem addFunction={(itemData) => {this.handleAddCompare(itemData)}} query={this.state.query} tableType={this.state.table} entryData={tableData}/>
                            </div>
                    }
                    })}
                </Row>}

                {/* ----------------------------------- Footer ----------------------------------- */}
                <Row>
                    <Col md="4">
                        <Pagination count={Math.ceil(this.state.items.length / this.state.pageSize)} page={this.state.page} onChange={this.handlePageChange} />
                    </Col>
                    <Col  md="3">
                        Page Size
                        {' '}
                        <select onChange={this.handleChangeRowsPerPage} value={this.state.pageSize}>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="25">25</option>
                        </select>
                    </Col>
                    <Col md="5">
                        <TextField style={{width: "70%"}} id="outlined-basic" label="Search" value={this.state.query} variant="outlined" onChange={this.handleSearch}/>
                        {' '}
                        <Button color={"primary"} style={{height: "100%"}} onClick={this.handleSearchSubmit}>Go</Button>
                    </Col>
                </Row>
                Showing {this.state.items.length} items
            </div>
        );
    }
}

export default GridComponent;
