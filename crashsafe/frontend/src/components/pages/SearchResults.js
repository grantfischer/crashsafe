import React from 'react';
import {
  Col,
  Row,
  Container,
  Table,
  Spinner
} from 'reactstrap';

import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import CarBrandInst from './CarBrandInst.js';
import CarModelInst from './CarModelInst.js';
import CrashTestInst from './CrashTestInst.js';
import ProviderInst from './ProviderInst.js';

import constants from "./../../static/constants.json"

const request = require('request');

var w = window.innerWidth;
var h = window.innerHeight;

class SearchResults extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
            query: props.match.params.query,

            loading: true,
            brandsLoading: true,
            modelsLoading: true,
            testsLoading: true,
            providersLoading: true,

            brandResults: null,
            modelResults: null,
            testResults: null,
            providerResults: null,

            brandRows: null,
            modelRows: null,
            testRows: null,
            providerRows: null,

		}
    }

    componentDidMount() {
        this.getBrandRows();
    }

    getBrandRows() {
		let url = constants.API_URL + "brands?query=" + this.state.query;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({brandResults: body})

			var rows = <React.Fragment>
				{this.state.brandResults.map((brand, i) => {
					return <tr>
						<th scope="row">{i + 1}</th>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/carbrand/${brand.id}`
							}}>{brand.name}</Link>
            			</td>
                        <td>{brand.year_founded}</td>
                        <td>{brand.model_count}</td>
					</tr>
				})}

			</React.Fragment>

            this.setState({brandRows: rows, brandsLoading: false})
            this.getModelRows();
		});	
	}

    getModelRows() {
		let url = constants.API_URL + "models?query=" + this.state.query;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({modelResults: body})

			var rows = <React.Fragment>
				{this.state.modelResults.map((carModel, i) => {
					return <tr>
					<th scope="row">{i + 1}</th>
						<td>{carModel.year}</td>
                        <td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/carbrand/${carModel.brand_id}`
							}}>{carModel.make}</Link>
            			</td>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/carmodel/${carModel.id}`
							}}>{carModel.model}</Link>
						</td>
					</tr>
				})}

			</React.Fragment>

            this.setState({modelRows: rows, modelsLoading: false})
            this.getTestRows();
        });	
        
    }
    
    getTestRows() {
        let url = constants.API_URL + "tests?query=" + this.state.query;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
            this.setState({testResults: body})

            var rows = <React.Fragment>
                {this.state.testResults.map((test, i) => {
                    return <tr>
                        <th scope="row">{i + 1}</th>
                        <td>
                            <Link to={location => {
                            var idx = location.pathname.lastIndexOf("/");
                            var newPath = location.pathname.substring(0, idx);
                            idx = location.pathname.lastIndexOf("/");
                            newPath = newPath.substring(0, idx);
                            location.pathname = newPath;
                            return `/crashtest/${test.id}`
                            }}>{test.num}</Link>
                        </td>
                        <td>{test.test_type}</td>
                        <td>
                            <Link to={location => {
                            var idx = location.pathname.lastIndexOf("/");
                            var newPath = location.pathname.substring(0, idx);
                            idx = location.pathname.lastIndexOf("/");
                            newPath = newPath.substring(0, idx);
                            location.pathname = newPath;
                            return `/provider/${test.performer_id}`
                            }}>{test.test_performer}</Link>
                        </td>
                    </tr>
                })}
            </React.Fragment>
            this.setState({testRows: rows, testsLoading: false});
            this.getProviderRows();
        });
    }

    getProviderRows() {
		let url = constants.API_URL + "providers?query=" + this.state.query;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({providerResults: body})

			var rows = <React.Fragment>
				{this.state.providerResults.map((provider, i) => {
					return <tr>
						<th scope="row">{i + 1}</th>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/provider/${provider.id}`
							}}>{provider.name}</Link>
						</td>
                        <td>{provider.location}</td>
                        <td>{provider.year_founded}</td>
                        <td>{provider.employee_count}</td>
                        <td>{provider.test_count}</td>
					</tr>
				})}
			</React.Fragment>
			this.setState({providerRows: rows, providersLoading: false, loading: false})
		});	
	}

	render() {
        // console.log(this.state)
		return (
            <Router>
				<Switch>
					<Route path="/carbrand/:id" component={CarBrandInst} />
					<Route path="/crashtest/:id" component={CrashTestInst} />
					<Route path="/carmodel/:id" component={CarModelInst} />
					<Route path="/provider/:id" component={ProviderInst} />
					<Route>
                    
                        <div style={{margin: "40px"}}>
                            {!this.state.loading && this.state.brandResults.length > 0 && <div style={{marginTop: "50px"}}>
                                <div style={{height: "75px"}}> <h1>Brands</h1></div>
                                <div style={{height: "400px", overflow: "scroll"}}>
                                    <Table striped>
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                            <th>Brand Name</th>
                                            <th>Year Founded</th>
                                            <th>Number of Models</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {!this.state.brandsLoading && this.state.brandRows}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>}

                            {!this.state.loading && this.state.modelResults.length > 0 && <div style={{marginTop: "50px"}}>
                                <div style={{height: "75px"}}> <h1>Models</h1></div>
                                <div style={{height: "400px", overflow: "scroll"}}>
                                    <Table striped>
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                            <th>Year</th>
                                            <th>Make</th>
                                            <th>Model</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {!this.state.modelsLoading && this.state.modelRows}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>}

                            {!this.state.loading && this.state.testResults.length > 0 && <div style={{marginTop: "50px"}}>
                                <div style={{height: "75px"}}> <h1>Tests</h1></div>
                                <div style={{height: "400px", overflow: "scroll"}}>
                                    <Table striped>
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                            <th>Test No.</th>
                                            <th>Test Type</th>
                                            <th>Test Performer</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {!this.state.testsLoading && this.state.testRows}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>}

                            {!this.state.loading && this.state.providerResults.length > 0 && <div style={{marginTop: "50px"}}>
                                <div style={{height: "75px"}}> <h1>Providers</h1></div>
                                <div style={{height: "400px", overflow: "scroll"}}>
                                    <Table striped>
                                        <thead>
                                            <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Location</th>
                                            <th>Year Founded</th>
                                            <th>No. Employees</th>
                                            <th>No. Tests</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {!this.state.providersLoading && this.state.providerRows}
                                        </tbody>
                                    </Table>
                                </div>
                            </div>}

                            {!this.state.loading && 
                                this.state.providerResults.length === 0 &&
                                this.state.testResults.length === 0 &&
                                this.state.brandResults.length === 0 &&
                                this.state.modelResults.length === 0 &&
                                <div style={{textAlign: "center", margin: "40px"}}><h1>No results found...</h1></div>}

                            {this.state.loading && <React.Fragment>
                                <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>
                            </React.Fragment>}

                        </div>

                    </Route>
                </Switch>
            </Router>
		);
	}
}
export default SearchResults;
