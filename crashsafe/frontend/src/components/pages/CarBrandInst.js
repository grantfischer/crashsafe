import React from 'react';
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  Jumbotron,
  Container,
  Card,
  CardTitle,
  CardSubtitle,
  Spinner,
  Button
} from 'reactstrap';
import { Rating } from '@material-ui/lab';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import { TwitterTimelineEmbed } from 'react-twitter-embed';

import { Img } from '@bootstrap-styled/v4';

import { FaTwitter } from 'react-icons/fa';

import CarModelInst from '../pages/CarModelInst'
import ProviderInst from '../pages/ProviderInst'
import constants from "./../../static/constants.json"


const request = require('request');

var w = window.innerWidth;
var h = window.innerHeight;
const collectionID = 9602202; //the collection ID from the original url

const logoCircleSize = 200;
const logoWidth = 140;
const logoTopMargin = (logoCircleSize * .5) - (logoWidth * .5);

class CarBrandInst extends React.Component {
	constructor(props) {
		super(props);
		console.log(props)
		this.state = {
			instance: props.match.params.id,
			data: null,
			loading: true,

			otherImgURL: "",
			url: "",

			models: null,
			modelsLoading: true,
			modelRows: null,

			providers: null,
			providersLoading: true,
			providerRows: null
		}
	}

	componentDidMount() {
		let url = constants.API_URL + "brand?id=" + this.state.instance;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body, loading: false})
			this.chooseBG();
		});	
	}

	componentWillUnmount() {
		this.setState({
			instance: null,
			data: null,
			loading: true,

			otherImgURL: "",
			url: "",

			models: null,
			modelsLoading: true,
			modelRows: null,

			providers: null,
			providersLoading: true,
			providerRows: null
		});
	}

	chooseBG(){
		fetch(`https://source.unsplash.com/collection/${collectionID}/${w}x${h}/`) 
		.then((response)=> { 
			this.setState({
				url: response.url
			});
			this.setOtherImgURL();
			this.getModelRows();
		})
	}

	setOtherImgURL() {
		const urlstr = Object(this.state.data).other_image;
		var str = "";
		if (urlstr) 
			str = "url(" + urlstr + ")";
		else
			str = "url(" + this.state.url + ")";
		this.setState({otherImgURL: str});
	}

	getModelRows() {
		const car = Object(this.state.data);
		let url = constants.API_URL + "models?brand=" + car.name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({models: body})

			var rows = <React.Fragment>
				{this.state.models.map((carModel, i) => {
					return <tr>
					<th scope="row">{i + 1}</th>
						<td>{carModel.year}</td>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/carmodel/${carModel.id}`
							}}>{carModel.model}</Link>
						</td>
					</tr>
				})}

			</React.Fragment>

			this.setState({modelRows: rows, modelsLoading: false})
			this.getProviderRows();
		});	
	}

	getProviderRows() {
		const car = Object(this.state.data);
		let url = constants.API_URL + "providers?brand=" + car.name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({providers: body})

			var rows = <React.Fragment>
				{this.state.providers.map((provider, i) => {
					return <tr>
						<th scope="row">{i + 1}</th>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/provider/${provider.id}`
							}}>{provider.name}</Link>
						</td>
					</tr>
				})}

			</React.Fragment>

			this.setState({providerRows: rows, providersLoading: false})
		});	
	}

	render() {
		const car = Object(this.state.data);
		console.log(this.state.data)
		return (
			<Router>
				<Switch>
				<Route path="/carmodel/:id" component={CarModelInst} />
				<Route path="/provider/:id" component={ProviderInst} />
				<Route>
					{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
					{!this.state.loading && <React.Fragment>

						<div style={{position: "absolute", width: "100%", height: "350px", background: "rgba(0, 0, 0, .5)"}} />
						<Jumbotron style={{backgroundImage: this.state.otherImgURL, height: "350px", backgroundPosition: "center center"}} />
						<div style={{"textAlign": "center"}}>
							<div style={{boxShadow: "5px 5px 15px grey", borderRadius: "50%", position: "absolute", marginTop: "-130px", marginLeft: "-100px", display: "inline-block", height: logoCircleSize, width: logoCircleSize, "text-align": "center", "background": "white"}}>
								{car.logo ? <img style={{marginTop: logoTopMargin}} src={car.logo} width={logoWidth} /> : 
								<div style={{marginTop: "90px"}}><h5>{car.name}</h5></div>}
							</div>
						</div>
						<br/>
						<br/>
						<br/>
						<br/>
						<Container>
							<Row>
								<Col md="4" style={{marginTop: "30px"}}>
									<h1><b>{car.name}</b></h1>
									{car.year_founded ? "Est. " + car.year_founded : ""}
									<br/><br/><br/>
								</Col>
								<Col md="4" style={{marginTop: "30px"}}>
									<h5>Number of Models: {car.model_count}</h5>
									{car.safety_rating && car.safety_rating != 0 ? <h5>Average Safety Rating: {car.safety_rating} <Rating name="half-rating-read" defaultValue={car.safety_rating} precision={0.01} readOnly /></h5> : ""}
									{car.user_rating && car.user_rating != 0 ? <h5>Average User Rating: {car.user_rating}</h5> : ""}
									<br/>
								</Col>
								<Col md="4" style={{textAlign: "center", marginTop: "30px"}}>
									{car.website ? <Button color="primary" size="lg" href={car.website}>Website</Button> : ""}
									{' '}
									{car.twitter_handle ? <Button color="primary" size="lg" href={"https://twitter.com/" + car.twitter_handle}><FaTwitter /></Button> : ""}
								</Col>							
								<br/><div style={{width:"100%"}} class="border-top my-3"></div><br/><br/>
								<Col md="6" style={{marginTop: "30px", height: "500px", overflow: "scroll"}}>
									<h6 style={{textAlign: "center"}}><b>{car.name} Models</b></h6><br/>
									<Table striped>
										<thead>
											<tr>
											<th>#</th>
											<th>Year</th>
											<th>Model</th>
											</tr>
										</thead>
										<tbody>
											{!this.state.modelsLoading && this.state.modelRows}
										</tbody>
									</Table>
								</Col>
								<Col md="6" style={{marginTop: "30px", height: "500px", overflow: "scroll"}}>
									<h6 style={{textAlign: "center"}}><b>Crash Test Providers that have tested {car.name}</b></h6><br/>
									<Table striped>
										<thead>
											<tr>
											<th>#</th>
											<th>Name</th>
											</tr>
										</thead>
										<tbody>
											{!this.state.providersLoading && this.state.providerRows}
										</tbody>
									</Table>
								</Col>
								{car.twitter_handle ? <React.Fragment><br/><div style={{width:"100%"}} class="border-top my-3"></div><br/><br/>
								<Col md="12" style={{textAlign: "center"}}>
									<TwitterTimelineEmbed
									sourceType="profile"
									screenName={car.twitter_handle.substring(1)}
									options={{width: "50%", height: 500}}
									/>
								</Col></React.Fragment> : ""}
							</Row>
							<br/><br/><br/>
						</Container>
					</React.Fragment>}
				</Route>
				</Switch>
			</Router>
		);
	}
}
export default CarBrandInst;
