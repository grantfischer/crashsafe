import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import {
  Col,
  Row,
  Table,
  Spinner
} from 'reactstrap';
import styled from 'styled-components';

import ProviderInst from "../pages/ProviderInst"
import TableComponent from "../TableComponent/TableComponent";
import GridComponent from "./GridComponent/GridComponent";
import constants from "./../../static/constants.json"


const request = require('request');

class Provider extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			loading: true
		};
	}

	componentDidMount() {
		let url = constants.API_URL + "providers"
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body, loading: false})
		});
	}

	render() {
		return (
			<React.Fragment>
				
				<Router>
					<Switch>
						<Route path={"/provider/:id"} component={ProviderInst} />
						<Route path="/provider/">
							<div style={{marginTop: "20px"}}>
							{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
								{!this.state.loading && <GridComponent table="providers" tableData={this.state.data} num={this.state.data.length}/>}
							</div>
						</Route>
					</Switch>
				</Router>
			</React.Fragment>
		);
	}
}

export default Provider;
