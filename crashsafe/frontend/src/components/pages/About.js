import React from 'react';
import {
  Col,
  Row,
} from 'reactstrap';
import Card from 'react-bootstrap/Card';
import styled from 'styled-components';

import alishaImg from "./../../assets/members/alisha.jpg";
import brandenImg from "./../../assets/members/branden.jpg";
import darrenImg from "./../../assets/members/darren.jpg";
import grantImg from "./../../assets/members/grant.jpg";
import gregImg from "./../../assets/members/greg.jpg";
import kirillImg from "./../../assets/members/kirill.jpg";
import imgGitlab from "./../../assets/gitlab.png";
import imgSlack from "./../../assets/slack.png";
import imgDocker from "./../../assets/docker.png";
import imgAws from "./../../assets/aws.png";
import imgJs from "./../../assets/js.png";
import imgReact from "./../../assets/react.png";
import imgMocha from "./../../assets/mocha.png";
import imgSelenium from "./../../assets/selenium.png";
import imgPython from "./../../assets/python.png";
import imgFlask from "./../../assets/flask.png";
import imgMysql from "./../../assets/mysql.svg";
import imgPostman from "./../../assets/postman.png";
import tests from "./../../static/member_tests.json"

const request = async (url: string, opts?: object) => {
  const response = await fetch(url, opts);
  const json = await response.json();

  if (response.ok) { return json; }

  const error = {
    ...json,
    status: response.status,
  };

  return error;
};

function ProfileCard(props) {
	return (
		<Card style={{ marginTop: '40px' }}>
			<Card.Img variant="top" src={props.img} />
			<Card.Body style={{minHeight: '150px'}}>
				<Card.Title>{props.name}</Card.Title>
				<Card.Subtitle className="mb-2 text-muted">{props.role}</Card.Subtitle>
				<Card.Text>{props.bio}</Card.Text>
			</Card.Body>
			<Card.Footer className="text-muted" style={{fontSize: '14px'}}>Commits: {props.stat.commits}, Issues: {props.stat.issues}, Tests: {props.stat.tests}</Card.Footer>
		</Card>
	);
}

function StatCard(props) {
	return (
		<Card style={{ marginTop: '10px', textAlign:'center'}}>
			<Card.Body>
				<Card.Subtitle style={{fontSize: '20px'}}>{props.name}: {props.count}</Card.Subtitle>
			</Card.Body>
		</Card>
	);
}

const Section = styled.div`
  width: 60%;
  margin: 40px auto 0px auto;
`;

const Paragraph = styled.p`
  margin-top: 30px;
  font-size: 20px;
`;


class About extends React.Component {
	constructor(props: any) {
		super(props);
		this.state = {
			commits: 0,
			issues: 0,
			tests: 0,
			stats: {
				alisha: {
					commits: 0,
					issues: 0,
					tests: 0
				},
				branden: {
					commits: 0,
					issues: 0,
					tests: 0
				},
				darren: {
					commits: 0,
					issues: 0,
					tests: 0
				},
				greg: {
					commits: 0,
					issues: 0,
					tests: 0
				},
				grant: {
					commits: 0,
					issues: 0,
					tests: 0
				},
				kirill: {
					commits: 0,
					issues: 0,
					tests: 0
				},
			},
		};
	}

	async componentDidMount() {
		var PROJECTID = 16946720;
		const members = {
			"alishazute": "Alisha Zut\u0233",
			"btj3991": "Branden James",
			"dsynkd": "Darren Sadr",
			"gpauloski": "Greg Pauloski",
			"grantfischer": "Grant Fischer",
			"kbelous": "Kirill Belous",
		};

		// COMMITS
		const commits = await request(`https://gitlab.com/api/v4/projects/${PROJECTID}/repository/contributors`);
		const commitCount = commits.reduce((commits: number, stat: any) => commits + stat.commits, 0);
		var commitMemberCount = {
			"Alisha Zute": 0,
			"Branden James": 0,
			"dsynkd": 0,
			"Greg Pauloski": 0,
			"Grant Fischer": 0,
			"Kirill Belous": 0,
		};
		for (var i = 0; i < commits.length; i++) {
			var commit = commits[i]
			for (var member in commitMemberCount) {
				if (commit.name.includes(member)) {
					commitMemberCount[member] = commit.commits;
				}
			}
		}

		// ISSUES
		const issues = await request(`https://gitlab.com/api/v4/projects/${PROJECTID}/issues_statistics`);
		const issuesCount = issues.statistics.counts.all;
		var issueMemberCount = {
			"Alisha Zute": 0,
			"Branden T James": 0,
			"Darren Sadr": 0,
			"Greg Pauloski": 0,
			"Grant Fischer": 0,
			"Kirill Belous": 0,
		};
		var memberIssue = null;
		for (var member in members) {
			memberIssue = await request(`https://gitlab.com/api/v4/projects/${PROJECTID}/issues_statistics?assignee_username=${member}&scope=all`);
			issueMemberCount[member] = memberIssue.statistics.counts.all;
		}

		// Aggregate stats into one final json object for easy use
		const stats = {
			alisha: {
				commits: commitMemberCount["Alisha Zute"],
				issues: issueMemberCount["alishazute"],
				tests: tests["alisha"],
			},
			branden: {
				commits: commitMemberCount["Branden James"],
				issues: issueMemberCount["btj3991"],
				tests: tests["branden"],
			},
			darren: {
				commits: commitMemberCount["dsynkd"],
				issues: issueMemberCount["dsynkd"],
				tests: tests["darren"],
			},
			greg: {
				commits: commitMemberCount["Greg Pauloski"],
				issues: issueMemberCount["gpauloski"],
				tests: tests["greg"],
			},
			grant: {
				commits: commitMemberCount["Grant Fischer"],
				issues: issueMemberCount["grantfischer"],
				tests: tests["grant"],
			},
			kirill: {
				commits: commitMemberCount["Kirill Belous"],
				issues: issueMemberCount["kbelous"],
				tests: tests["kirill"],
			},
		};

		this.setState({
			commits: commitCount,
			issues: issuesCount,
			tests: Object.values(tests).reduce((acc, cur) => acc + cur, 0),
			stats: stats,
		});
	}

	render() {
		const { 
			commits,
			issues,
			tests,
			stats,
		} = this.state;

		return (
			<div>
				{/*ABOUT US SECTION*/}
				<Section>
					<Row>
						<Col>
							<h1>About Us</h1>
							<Paragraph>
								CrashSafe is the convenient place to find all the safety information on cars before you buy.
								Our database combines car manufacturer specifications with independent third-party crash test data
								so you can be confident the car you drive off the lot will protect no matter what happens.
							</Paragraph>
						</Col>
					</Row>
				</Section>
				{/*MEMBERS SECTION*/}
				<Section>
					<Row>
						<Col>
							<h1>Members</h1>
							<Row>
								<Col>
									<ProfileCard 
										name="Alisha Zut&#233;" 
										role="Back-end, Databases, Data Gathering" 
										img={alishaImg} 
										bio="2nd-year UTCS student trying her very best" 
										stat={stats.alisha}/>
								</Col>
								<Col>
									<ProfileCard 
										name="Branden James" 
										role="Back-end, API, Source Research, Backend Testing, Databases" 
										img={brandenImg} 
										bio="Undergraduate in Computer Science and TA for Ahmed Gheith's CS 439" 
										stat={stats.branden}/>
								</Col>
							</Row>
							<Row>
								<Col>
									<ProfileCard 
										name="Darren Sadr" 
										role="Front-End, Database, Multimedia" 
										img={darrenImg} 
										bio="Junior Computer Science student, fitness enthusiast" 
										stat={stats.darren}/>
								</Col>
								<Col>
									<ProfileCard 
										name="Grant Fischer" 
										role="Full-Stack, Front-End: Design and Implementation" 
										img={grantImg} 
										bio="UTCS Junior from Dallas, likes wakeboarding and sailing" 
										stat={stats.grant}/>
								</Col>
							</Row>
							<Row>
								<Col>
									<ProfileCard 
										name="Greg Pauloski" 
										role="Hosting, Dev-Ops, Testing, Databases" 
										img={gregImg} 
										bio="CS undergraduate interested in the intersection of machine learning and HPC" 
										stat={stats.greg}/>
								</Col>
								<Col>
									<ProfileCard 
										name="Kirill Belous" 
										role="Front-end, Databases, Selenium Testing, Data Scraping" 
										img={kirillImg} 
										bio="4th-Year Computer Science Student at UT Austin" 
										stat={stats.kirill}/>
								</Col>
							</Row>
						</Col>
					</Row>
				</Section>
				{/*Repository*/}
				<Section>
					<Row>
						<Col>
							<h1>Development</h1>
						</Col>
					</Row>
					{/*GitLab Stats*/}
					<Paragraph>
						<Row>
							<Col>
								<h3>Repository Statistics (<a href="https://gitlab.com/grantfischer/crashsafe">GitLab</a>)</h3>
							</Col>
						</Row>
						<Row>
							<Col>
								<StatCard name="Commits" count={commits} />
							</Col>
							<Col>
								<StatCard name="Issues" count={issues} />
							</Col>
							<Col>
								<StatCard name="Tests" count={tests} />
							</Col>
						</Row>
					</Paragraph>
					{/*Data Sources*/}
					<Paragraph>
						<Row>
							<Col>
								<h3>Data Sources</h3>
								Our data is gathered from the following sources:
								<ul>
									<li><a href="https://www-nrd.nhtsa.dot.gov/database/veh/veh.htm">NHTSA Vehicle Crash Test Database</a></li>
									<li><a href="https://webapi.nhtsa.gov/Default.aspx?SafetyRatings/API/5">NHTSA New Car Assessment Program</a></li>
									<li><a href="https://www-fars.nhtsa.dot.gov/Main/index.aspx">NHTSA Fatality Analysis Reporting System</a></li>
									<li><a href="https://databases.one/car-make-model-database-api/#specs">Car Database API</a></li>
									<li><a href="https://api.iihs.org/">Insurance Institute for Highway Safety</a></li>
								</ul>
								The majority of data was scraped from these websites using Python scripts that query their API and convert the data appropriately for our database.
								For some of the NHTSA data, the data was downloaded directly as PSV files and then Python scripts were written to import the data into our database.
							</Col>
						</Row>
					</Paragraph>
					{/*API*/}
					<Paragraph>
						<Row>
							<Col>
								<h3>CrashSafe API</h3>
								<a href="https://documenter.getpostman.com/view/10416711/SzKYNw2g?version=latest">Postman API Docs</a>
							</Col>
						</Row>
					</Paragraph>
					{/*Tools*/}
					<h3>Tools</h3>
					<Paragraph>
						<Row>
							<Col><a href="https://www.gitlab.com"><img src={imgGitlab} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.slack.com"><img src={imgSlack} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.docker.com"><img src={imgDocker} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://aws.amazon.com"><img src={imgAws} style={{width: "80%"}} /></a></Col>
						</Row>
						<Row>
							<Col><a href="https://www.javascript.com"><img src={imgJs} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.reactjs.org"><img src={imgReact} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.mochajs.org"><img src={imgMocha} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.selenium.dev"><img src={imgSelenium} style={{width: "80%"}} /></a></Col>
						</Row>
						<Row>
							<Col><a href="https://python.org"><img src={imgPython} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://flask.palletsprojects.com/en/1.1.x/"><img src={imgFlask} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.mysql.com"><img src={imgMysql} style={{width: "80%"}} /></a></Col>
							<Col><a href="https://www.postman.com"><img src={imgPostman} style={{width: "80%"}} /></a></Col>
						</Row>
					</Paragraph>
					{/*Presentation*/}
					<br /><br /><h3>Presentation</h3><br />
					<Row>
						<Col style={{textAlign: "center"}}>
							<iframe width="100%" height="500px" src="https://www.youtube.com/embed/ZEVOplfGteA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</Col>
					</Row>
					<br /><br />
				</Section>
      		</div>
		);
	}
}
export default About;
