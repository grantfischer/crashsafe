import React from 'react'

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import DangerZonePopMap from '../Visualizations/DangerZonePopMap.js'
import AirPollution from '../Visualizations/AirPollution.js'
import AvgRatingByType from '../Visualizations/AvgRatingByType.js'
import AvgRatingByYear from '../Visualizations/AvgRatingByYear.js'
import DangerZoneMurderChart from '../Visualizations/DangerZoneMurderChart'
import RatingsScatterplot from '../Visualizations/RatingsScatterplot'

function TabPanel(props) {
   const { children, value, index, ...other } = props;
 
   return (
     <div
       role="tabpanel"
       hidden={value !== index}
       id={`simple-tabpanel-${index}`}
       aria-labelledby={`simple-tab-${index}`}
       {...other}
     >
       {value === index && (
         <Box p={3}>
           <Typography>{children}</Typography>
         </Box>
       )}
     </div>
   );
 }

function a11yProps(index) {
   return {
     id: `simple-tab-${index}`,
     'aria-controls': `simple-tabpanel-${index}`,
   };
 }

class Visualizations extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         tab: 0
      }
   }

   handleChange = (event, newValue) => {
      this.setState({tab: newValue});
    };

   render() {
      return (
         <div>
            <AppBar position="static">
               <Tabs style={{background: "grey"}} value={this.state.tab} onChange={this.handleChange} aria-label="simple tabs example">
                  <Tab label="CrashSafe" {...a11yProps(0)} />
                  <Tab label="DangerZone" {...a11yProps(1)} />
               </Tabs>
            </AppBar>
            <TabPanel value={this.state.tab} index={0}>
               <center>
                  <h1>CrashSafe Visualizations</h1>
                  <h2>Average Vehicle Rating by Year</h2>
                    <AvgRatingByYear/>
                  <h2>Average Vehicle Rating by Type</h2>
                    <AvgRatingByType/>
                  <h2>Make Rating v Model Rating</h2>
                    <RatingsScatterplot/>
               </center>
            </TabPanel>
            <TabPanel value={this.state.tab} index={1}>
               <center>
                  <h1>DangerZone Visualizations</h1>
                  <br/><br/>
                  <h2>US City Population Size</h2>
                     <svg width="960" height="600">
                        <DangerZonePopMap width={960} height={600}/>
                     </svg>
                  <br/><br/><br/>
                  <h2>Air Pollution vs. Life Expectancy</h2>
                     <AirPollution/>
                  <br/><br/><br/>
                  <h2>Murder Rates by City</h2>
                     <DangerZoneMurderChart/>
               </center>
            </TabPanel>
         </div>
    );

   }
}
export default Visualizations;
