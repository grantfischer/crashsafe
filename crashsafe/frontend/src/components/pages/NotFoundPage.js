import React from 'react';
import { Link } from 'react-router-dom';
import PageNotFound from '../../assets/NotFoundPage.png';

class NotFoundPage extends React.Component{
    render() {
        return (
            <div>
            <center>
                <img align="middle" height="400" src={PageNotFound} />
                <p style={{textAlign:"center"}}>
                    <h2>Page not found</h2>
                </p>
            </center>
            </div>
        )
    }
}
export default NotFoundPage;