import React from 'react';
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  Jumbotron,
  Container,
  Card,
  CardTitle,
  CardSubtitle,
  Spinner,
  Button
} from 'reactstrap';
import { Rating } from '@material-ui/lab';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import CarBrandInst from '../pages/CarBrandInst'
import CrashTestInst from '../pages/CrashTestInst'
import ProviderInst from '../pages/ProviderInst'
import constants from "./../../static/constants.json"

import { TwitterTimelineEmbed } from 'react-twitter-embed';

const request = require('request');

var w = window.innerWidth;
var h = window.innerHeight;
const collectionID = 9602202; //the collection ID from the original url

const logoCircleSize = 200;
const logoWidth = 140;
const logoTopMargin = (logoCircleSize * .5) - (logoWidth * .5);

class CarModelInst extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			instance: props.match.params.id,
			data: null,
			loading: true,

			tests: null,
			testsLoading: true,
			testRows: null,

			brandID: null,
			brandLogo: null,
			tempProviderID: {},

			twitter_handle: null
		}
	}

	componentDidMount() {
		let url = constants.API_URL + "model?id=" + this.state.instance;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body})
			this.chooseBG();
		});
	}

	chooseBG(){
		fetch(`https://source.unsplash.com/collection/${collectionID}/${w}x${h}/`) 
		.then((response)=> { 
			this.setState({
				url: response.url
			});
			this.setOtherImgURL();
			this.getTestRows();
		})
	}

	setOtherImgURL() {
		const urlstr = Object(this.state.data).other_image;
		var str = "";
		if (urlstr) 
			str = "url(" + urlstr + ")";
		else
			str = "url(" + this.state.url + ")";
		this.setState({otherImgURL: str});
	}

	getProviderID(name) {
		let url = constants.API_URL + "provider?name=" + name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.state.tempProviderID[name] = body.id;
		});
	}

	setBrand(name) {
		let url = constants.API_URL + "brand?name=" + name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({brandID: body.id, brandLogo: body.logo, twitter_handle: body.twitter_handle, loading: false});
		});
	}

	getTestRows() {
		const car = Object(this.state.data);
		let url = constants.API_URL + "tests?model=" + car.id;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({tests: body})

			var rows = <React.Fragment>
				{this.state.tests.map((test, i) => {
					return <tr>
					<th scope="row">{i + 1}</th>
						<td>
							<Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/crashtest/${test.id}`
							}}>{test.num}</Link>
						</td>
						<td>{test.test_type}</td>
						<td>
							<Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/provider/${test.performer_id}`
							}}>{test.test_performer}</Link>
						</td>
					</tr>
				})}

			</React.Fragment>

			this.setState({testRows: rows, testsLoading: false})
			this.setBrand(this.state.data.make);
		});	
	}

	render() {
		const car = Object(this.state.data);
		console.log(this.state)
		return (
			<Router>
				<Switch>
					<Route path="/carbrand/:id" component={CarBrandInst} />
					<Route path="/crashtest/:id" component={CrashTestInst} />
					<Route path="/provider/:id" component={ProviderInst} />
					<Route>

						{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
						{!this.state.loading && <React.Fragment>

							<div style={{position: "absolute", width: "100%", height: "350px", background: "rgba(0, 0, 0, .5)"}} />
							<Jumbotron style={{backgroundImage: this.state.otherImgURL, height: "350px", backgroundPosition: "center center"}} />
							<div style={{"textAlign": "center"}}>
								<div style={{boxShadow: "5px 5px 15px grey", borderRadius: "50%", position: "absolute", marginTop: "-130px", marginLeft: "-100px", display: "inline-block", height: logoCircleSize, width: logoCircleSize, "text-align": "center", "background": "white"}}>
									{this.state.brandLogo ? <img style={{marginTop: logoTopMargin}} src={this.state.brandLogo} width={logoWidth} /> : 
									<div style={{marginTop: "90px"}}><h5>{car.year} {car.make} {car.model}</h5></div>}
								</div>
							</div>
							<br/>
							<br/>
							<br/>
							<br/>
							<Container>
								<Row>
									<Col md="6" style={{marginTop: "30px"}}>
										<h1>
											<b>
												{car.year}
												{' '}
												<Link to={location => {
												var idx = location.pathname.lastIndexOf("/");
												var newPath = location.pathname.substring(0, idx);
												idx = location.pathname.lastIndexOf("/");
												newPath = newPath.substring(0, idx);
												location.pathname = newPath;
												return `/carbrand/${this.state.brandID}`
												}}>{car.make}</Link>
												{' '}
												{car.model}
											</b>
										</h1>
										{car.vehicle_type ? "Type: " + car.vehicle_type : ""}
										<br/><br/><br/>

										{car.engine_type ? <h5>Engine Type: {car.engine_type}</h5> : ""}
										{car.horse_power ? <h5>Horse Power: {car.horse_power}</h5> : ""}
										<br/><br/>
										{(car.IIHS_frontModerateOverlap != "N/A" && car.IIHS_frontModerateOverlap) || (car.IIHS_frontSmallOverlap != "N/A" && car.IIHS_frontSmallOverlap) || (car.IIHS_side != "N/A" && car.IIHS_side) || (car.IIHS_rollover != "N/A" && car.IIHS_rollover) || (car.IIHS_rear != "N/A" && car.IIHS_rear) ? <h4><b>IIHS Ratings</b></h4> : ""}
										{car.IIHS_frontModerateOverlap && car.IIHS_frontModerateOverlap != "N/A" ? <h5>Front Moderate Overlap: {car.IIHS_frontModerateOverlap}</h5> : ""}
										{car.IIHS_frontSmallOverlap && car.IIHS_frontSmallOverlap != "N/A" ? <h5>Front Small Overlap: {car.IIHS_frontSmallOverlap}</h5> : ""}
										{car.IIHS_side && car.IIHS_side != "N/A" ? <h5>Side: {car.IIHS_side}</h5> : ""}
										{car.IIHS_rollover && car.IIHS_rollover != "N/A" ? <h5>Rollover: {car.IIHS_rollover}</h5> : ""}
										{car.IIHS_rear && car.IIHS_rear != "N/A" ? <h5>Rear: {car.IIHS_rear}</h5> : ""}
										<br/><br/>
										{car.NHTSA_overall_rating || car.NHTSA_overall_side_crash_rating || car.NHTSA_overall_front_crash_rating || car.NHTSA_rollover_rating ? <h4><b>NHTSA Ratings</b></h4> : ""}
										{car.NHTSA_overall_rating && car.NHTSA_overall_rating != 0 ? <h5>Overall: <Rating name="read-only" value={car.NHTSA_overall_rating} readOnly /></h5> : ""}
										{car.NHTSA_overall_side_crash_rating && car.NHTSA_overall_side_crash_rating != 0 ? <h5>Overall Side Crash: <Rating name="read-only" value={car.NHTSA_overall_side_crash_rating} readOnly /></h5> : ""}
										{car.NHTSA_overall_front_crash_rating && car.NHTSA_overall_front_crash_rating != 0 ? <h5>Overall Front Crash: <Rating name="read-only" value={car.NHTSA_overall_front_crash_rating} readOnly /></h5> : ""}
										{car.NHTSA_rollover_rating && car.NHTSA_rollover_rating != 0 ? <h5>Rollover: <Rating name="read-only" value={car.NHTSA_rollover_rating} readOnly /></h5> : ""}
										<br/>

									</Col>
									<Col md="6" style={{marginTop: "30px"}}>
										{car.img ? <div md="6" style={{textAlign: "center"}}>
											<img src={car.img} width="100%" style={{boxShadow: "5px 5px 15px grey"}}></img>
											<br/><br/><br/><br/>
										</div> : ""}
									</Col>
									{this.state.twitter_handle ? <Col md="6" style={{textAlign: "center"}}>
										<TwitterTimelineEmbed
										sourceType="profile"
										screenName={this.state.twitter_handle.substring(1)}
										options={{width: "100%", height: 400}}
										/>
									</Col> : ""}
									<Col md="6" style={{marginTop: "30px"}}>
										<div style={{height: "350px", overflow: "scroll"}}>
											<h6 style={{textAlign: "center"}}><b>{car.year} {car.make} {car.model} Crash Tests</b></h6><br/>
											<Table striped>
												<thead>
													<tr>
													<th>#</th>
													<th>Test No.</th>
													<th>Test Type</th>
													<th>Test Performer</th>
													</tr>
												</thead>
												<tbody>
													{!this.state.testsLoading && this.state.testRows}
												</tbody>
											</Table>
										</div>
									</Col>
								</Row>
								<br/><br/><br/>
								</Container>
						</React.Fragment>}
					</Route>
				</Switch>
			</Router>
		);
	}
}
export default CarModelInst;
