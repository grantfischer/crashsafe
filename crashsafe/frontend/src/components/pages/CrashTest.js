import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import {
  Col,
  Row,
  Table,
  Spinner
} from 'reactstrap';
import styled from 'styled-components';

import CrashTestInst from "../pages/CrashTestInst"
import CarModelInst from "../pages/CarModelInst"
import ProviderInst from "../pages/ProviderInst"
import CarBrandInst from "../pages/CarBrandInst"
import TableComponent from "../TableComponent/TableComponent";
import constants from "./../../static/constants.json"


const request = require('request');

class CrashTest extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			loading: true
		};
	}

	componentDidMount() {
		let url = constants.API_URL + "tests"
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body, loading: false})
		});
	}

	componentDidUpdate() {
		console.log("update");
		let highlighter = function(e){
			console.log("highlight");
			setTimeout(function(){ // wait for keydown to change val
				let val = e.srcElement.value;
				let tbl = document.querySelectorAll('td.MuiTableCell-root.MuiTableCell-body');
				if(tbl == null) return;
				tbl.forEach(function(el){
					let tmp = el.innerText;
					let re = new RegExp(val, 'gi');
					let newval = tmp.match(re);
					tmp = tmp.replace(newval, '<span style="background-color: yellow">' + newval + '</span>');
					if(el.innerHTML.includes("href"))
						el.querySelector("a").innerHTML = tmp;
					else
						el.innerHTML = tmp;
				});
			}, 50);
		}
		setTimeout(function(){ // prevent test_js error
			let searchbox = document.querySelector('input.MuiInputBase-input');
			if(searchbox == null) return;
			searchbox.onkeydown = highlighter;
		}, 50);
	}

	render() {
		return (
			<React.Fragment>
				
				<Router>
					<Switch>
						<Route path={"/crashtest/:id"} component={CrashTestInst} />
						<Route path={"/carbrand/:id"} component={CarBrandInst} />
						<Route path={"/carmodel/:id"} component={CarModelInst} />
						<Route path={"/provider/:id"} component={ProviderInst} />
						<Route path="/crashtest/">
							<div style={{marginTop: "20px"}}>
							{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
								{!this.state.loading && <TableComponent table="tests" tableData={this.state.data} num={this.state.data.length}/>}
							</div>
						</Route>
					</Switch>
				</Router>
			</React.Fragment>
		);
	}
}
export default CrashTest;
