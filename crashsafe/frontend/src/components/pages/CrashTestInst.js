import React from 'react';
import {
  Col,
  Row,
  Jumbotron,
  Container,
  Spinner,
} from 'reactstrap';
import { Rating } from '@material-ui/lab';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import CarBrandInst from '../pages/CarBrandInst'
import CarModelInst from '../pages/CarModelInst'
import ProviderInst from '../pages/ProviderInst'
import constants from "./../../static/constants.json"

const request = require('request');

var w = window.innerWidth;
var h = window.innerHeight;
const collectionID = 9602202; //the collection ID from the original url

const logoCircleSize = 200;
const logoWidth = 140;
const logoTopMargin = (logoCircleSize * .5) - (logoWidth * .5);

class CrashTestInst extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			instance: props.match.params.id,
			data: null,
			loading: true,
			
			url: null,
			otherImgURL: null,

			providerID: null,
			providerLogo: null,
		}
	}

	componentDidMount() {
		let url = constants.API_URL + "test?id=" + this.state.instance;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body, loading: false})
		});
		this.setProviderInfo();
	}

	chooseBG(){
		fetch(`https://source.unsplash.com/collection/${collectionID}/${w}x${h}/`) 
		.then((response)=> { 
			this.setState({
				url: response.url
			});
			this.setOtherImgURL();
		})
	}

	setOtherImgURL() {
		const urlstr = this.state.otherImgURL;
		var str = "";
		if (urlstr) 
			str = "url(" + urlstr + ")";
		else
			str = "url(" + this.state.url + ")";
		this.setState({otherImgURL: str});
	}

	setProviderInfo() {
		const crashtest = Object(this.state.data);
		let url = constants.API_URL + "provider?name=" + crashtest.test_performer;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({providerID: body.id, providerLogo: body.logo, otherImgURL: body.other_image})
		});
		this.chooseBG();
	}

	getImageSources() {
		const imgCount = this.state.data.pic_count;
		let rowElements = [];
		for(var i = 6; i <= imgCount && i <= 15; ++i){
			var imgUrl = "https://www-nrd.nhtsa.dot.gov/database/MEDIA/GetMedia.aspx?tstno=" + this.state.data.num + "&index=" + i + "&database=V&type=P"
			rowElements.push(<img style={{width: "45%", margin: "20px", display: "inline-block"}} src={imgUrl} onError={(e) => e.target.style.display = "none"} />)
		}
		return rowElements;
	}

	getVideoSources() {
		const vidCount = this.state.data.vid_count;
		let rowElements = [];
		for(var i = 1; i <= vidCount; ++i) {
			var vidUrl = "https://www-nrd.nhtsa.dot.gov/database/MEDIA/GetMedia.aspx?tstno=" + this.state.data.num + "&index=" + i + "&database=V&type=C"
			rowElements.push(<li><a href={vidUrl}>Video {i}</a></li>)
		}
		return rowElements;
	}

	render() {
		const crashtest = Object(this.state.data);
		// console.log(this.state.data)
		return (
			<Router>
				<Switch>
					<Route path="/carbrand/:id" component={CarBrandInst} />
					<Route path="/carmodel/:id" component={CarModelInst} />
					<Route path="/provider/:id" component={ProviderInst} />
					<Route>
						{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
						{!this.state.loading && <React.Fragment>

							<div style={{position: "absolute", width: "100%", height: "350px", background: "rgba(0, 0, 0, .5)"}} />
							<Jumbotron style={{backgroundImage: this.state.otherImgURL, height: "350px", backgroundPosition: "center center"}} />
							<div style={{"textAlign": "center"}}>
								<div style={{boxShadow: "5px 5px 15px grey", borderRadius: "50%", position: "absolute", marginTop: "-130px", marginLeft: "-100px", display: "inline-block", height: logoCircleSize, width: logoCircleSize, "text-align": "center", "background": "white"}}>
									{this.state.providerLogo ? <img style={{marginTop: logoTopMargin}} src={this.state.providerLogo} width={logoWidth} /> : 
									<div style={{marginTop: "90px"}}><h5>#{crashtest.num}</h5></div>}
								</div>
							</div>
							<br/>
							<br/>
							<br/>
							<br/>
							<Container>
								<Row>
									<Col md="6" style={{marginTop: "30px"}}>
										<h1><b>Crash Test #{crashtest.num}</b></h1>
										<h5>{crashtest.test_type ? "Type: " + crashtest.test_type : ""}</h5>
										<h5>{crashtest.test_performer ? "Performed by: " : ""}
										{crashtest.test_performer ?
											<Link to={location => {
											var idx = location.pathname.lastIndexOf("/");
											var newPath = location.pathname.substring(0, idx);
											idx = location.pathname.lastIndexOf("/");
											newPath = newPath.substring(0, idx);
											location.pathname = newPath;
											return `/provider/${crashtest.performer_id}`
											}}>{crashtest.test_performer}</Link> : ""}</h5>
										<br/>
										<h5>{crashtest.model_id_1 ? "Vehicle 1: " : ""}
										{crashtest.model_id_1 ? <Link to={location => {
											var idx = location.pathname.lastIndexOf("/");
											var newPath = location.pathname.substring(0, idx);
											idx = location.pathname.lastIndexOf("/");
											newPath = newPath.substring(0, idx);
											location.pathname = newPath;
											return `/carmodel/${crashtest.model_id_1}`
											}}>{crashtest.car_1_year} {crashtest.car_1_make} {crashtest.car_1_model}</Link> : ""}</h5>
										<h5>{crashtest.model_id_2 ? "Vehicle 2: " : ""}
										{crashtest.model_id_2 ? <Link to={location => {
											var idx = location.pathname.lastIndexOf("/");
											var newPath = location.pathname.substring(0, idx);
											idx = location.pathname.lastIndexOf("/");
											newPath = newPath.substring(0, idx);
											location.pathname = newPath;
											return `/carmodel/${crashtest.model_id_2}`
											}}>{crashtest.car_2_year} {crashtest.car_2_make} {crashtest.car_2_model}</Link> : ""}</h5>
									</Col>
									<Col md="6" style={{marginTop: "30px"}}>
										<h4><b>Variables {'&'} Results</b></h4>
										{crashtest.impact_angle ? <h5>Impact Angle: {crashtest.impact_angle}&deg;</h5> : <h5>Impact Angle: 0&deg;</h5>}
										{crashtest.model_id_1 ? <h5>Vehicle 1 Speed: {crashtest.collision_speed} km/h</h5> : ""}
										{crashtest.model_id_2 ? <h5>Vehicle 2 Speed: {crashtest.collision_speed_2} km/h</h5> : ""}
										{crashtest.model_id_1 ? <h5>Vehicle 1 Max Crush: {crashtest.max_crush} mm</h5> : ""}
										{crashtest.model_id_2 ? <h5>Vehicle 2 Max Crush: {crashtest.max_crush_2} mm</h5> : ""}
									</Col>
									<br/><div style={{width:"100%"}} class="border-top my-3"></div><br/><br/>
									<Col md="10" style={{marginTop: "30px"}}>
										Photos ({crashtest.pic_count}): <br />{ this.getImageSources() }
									</Col>
									<Col md="2" style={{marginTop: "30px"}}>
										Videos ({crashtest.vid_count}): <ul>{ this.getVideoSources() }</ul>
									</Col>
								</Row>
							</Container>
						</React.Fragment>}
					</Route>
				</Switch>
			</Router>
		);
	}
}
export default CrashTestInst;
