import React from 'react';
import {
  Col,
  Row,
  Container,
  Input,
} from 'reactstrap';
import {
	BrowserRouter as Router,
	Switch,
	Route,
} from "react-router-dom";

import Background from "../Background";
import CarBrandInst from './CarBrandInst.js';
import CarModelInst from './CarModelInst.js';
import CrashTestInst from './CrashTestInst.js';
import ProviderInst from './ProviderInst.js';

import styled from 'styled-components';

var w = window.innerWidth;
var h = window.innerHeight;

const collectionID = 9602202; //the collection ID from the original url


const Title = styled.p`
	font-size: 84px;
	margin-top: 100px;
	margin-bottom: 20px;
	color: white;
	text-shadow: 5px 5px 7px #121212;
`
const Paragraph = styled.p`
  margin-top: 0px;
  font-size: 20px;
  color: white;

  text-shadow: 2px 2px 6px #121212;
`;

const TitleSmall = styled.p`
	font-size: 60px;
	margin-top: 100px;
	margin-bottom: 20px;
	color: white;
	text-shadow: 3px 3px 5px #121212;
`

const ParagraphSmall = styled.p`
  margin-top: 0px;
  font-size: 15px;
  color: white;
  spacing: 10px;
  line-height: 20px;
  text-shadow: 1px 1px 3px #121212;
`;

const Button = styled.a`
  display: inline-block;
  color: white;
  background: transparent;
  font-size: 22px;
  margin: 1em;
  padding: .3em 1.3em;
  border: 3px solid white;
  border-radius: 3px;
  text-shadow: 2px 2px 6px #121212;

  &:hover {
	color: white;
	border: 3px solid #24C6DC;
	text-decoration: none;

`;

class Home extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			url: "",
			width: w,
			height: h,
			query: "",
		}
	}

	/**
	 * Calculate & Update state of new dimensions
	 */
	updateDimensions() {
		let update_width  = window.innerWidth;
		let update_height = window.innerHeight;
		this.setState({ width: update_width, height: update_height });
	}

	componentDidMount() {
		this.updateDimensions();
		window.addEventListener("resize", this.updateDimensions.bind(this));
	
		this.chooseBG();
	}

	componentWillUnmount() {
		window.removeEventListener("resize", this.updateDimensions.bind(this));
	}

	chooseBG(){
		fetch(`https://source.unsplash.com/collection/${collectionID}/${w}x${h}/daily`) 
		.then((response)=> { 
			this.setState({
				url: response.url
			});
		})
	}

	updateQuery = (event) => {
		console.log(event.target.value);
		this.setState({query: event.target.value});
	}

	render() {
		return (
			<Router>
				<Switch>
					<Route path="/carbrand/:id" component={CarBrandInst} />
					<Route path="/crashtest/:id" component={CrashTestInst} />
					<Route path="/carmodel/:id" component={CarModelInst} />
					<Route path="/provider/:id" component={ProviderInst} />
					<Route>
						<Background src={this.state.url} />
						<Container>
							<Row>
								{window.innerWidth <= 500 ? <React.Fragment>
									<Col md="12" style={{"text-align": "center"}} className="App-header">
										<TitleSmall>
											<p><b>CrashSafe</b></p>
										</TitleSmall>
										<ParagraphSmall>
											<p><b>Information about car models, specs, and safety ratings, all in one place.</b></p>
										</ParagraphSmall>
										<br />
										{/* <Button href="/carbrand" style={{"background": "#0c2340"}}>Get Started</Button> */}
									</Col>
								</React.Fragment> : <React.Fragment>
									<Col md="12" style={{"textAlign": "center"}} className="App-header">
										<Title>
											<b>CrashSafe</b>
										</Title>
										<Paragraph>
											<b>Information about car models, specs, and safety ratings, all in one place.</b>
										</Paragraph>
										<br />
										{/* <Button href="/carbrand" style={{"background": "#0c2340"}}>Get Started</Button> */}
									</Col>
								</React.Fragment>}
							</Row>
							<br/>
							<br/>
							<br/>
							<Row style={{textAlign: "center"}}>
								<Col style={{textAlign: "center"}}>
									<Input type="text" name="search" id="searchbar" placeholder="Search Site" onChange={this.updateQuery} />
									<Button href={"/search/" + this.state.query}>Search</Button>
								</Col>
							</Row>
						</Container>
					</Route>
				</Switch>
			</Router>
		);
	}
}
export default Home;
