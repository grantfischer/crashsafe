import React from 'react';
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  Jumbotron,
  Container,
  Card,
  CardTitle,
  CardSubtitle,
  Spinner,
  Button
} from 'reactstrap';
import { Rating } from '@material-ui/lab';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import { Img } from '@bootstrap-styled/v4';

import { FaTwitter } from 'react-icons/fa';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

import CarModelInst from '../pages/CarModelInst'
import CarBrandInst from '../pages/CarBrandInst'
import CrashTestInst from '../pages/CrashTestInst'
import constants from "./../../static/constants.json"


const request = require('request');

var w = window.innerWidth;
var h = window.innerHeight;
const collectionID = 9602202; //the collection ID from the original url

const logoCircleSize = 200;
const logoWidth = 140;
const logoTopMargin = (logoCircleSize * .5) - (logoWidth * .5);

class ProviderInst extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			instance: props.match.params.id,
			data: null,
			loading: true,

			otherImgURL: "",
			url: "",

			models: null,
			modelsLoading: true,
			modelRows: null,

			tests: null,
			testsLoading: true,
			testRows: null,

			modelDict: {},
		}
	}

	componentDidMount() {
		let url = constants.API_URL + "provider?id=" + this.state.instance;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({data: body})
			this.chooseBG();
		});
	}

	chooseBG(){
		fetch(`https://source.unsplash.com/collection/${collectionID}/${w}x${h}/`) 
		.then((response)=> { 
			this.setState({
				url: response.url
			});
			this.setOtherImgURL();
			this.getModelRows();
		})
	}

	setOtherImgURL() {
		const urlstr = Object(this.state.data).other_image;
		var str = "";
		if (urlstr) 
			str = "url(" + urlstr + ")";
		else
			str = "url(" + this.state.url + ")";
		this.setState({otherImgURL: str});
	}

	getModelRows() {
		const tester = Object(this.state.data);
		let url = constants.API_URL + "models?provider=" + tester.name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			this.setState({models: body});

			var mdlDict = {};
			this.state.models.map((carModel, i) => {
				var tmpList = [];
				tmpList[0] = carModel.year;
				tmpList[1] = carModel.make;
				tmpList[2] = carModel.model;
				mdlDict[carModel.id] = tmpList;
			});
			this.setState({modelDict: mdlDict})

			var rows = <React.Fragment>
				{this.state.models.map((carModel, i) => {
					return <tr>
						<th scope="row">{i + 1}</th>
						<td>{carModel.year}</td>
						<td>{carModel.make}</td>
						<td><Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/carmodel/${carModel.id}`
							}}>{carModel.model}</Link>
						</td>
					</tr>
				})}

			</React.Fragment>

			this.setState({modelRows: rows, modelsLoading: false})
			this.getTestRows();
		});	
	}

	getTestRows() {
		const tester = Object(this.state.data);
		let url = constants.API_URL + "tests?provider=" + tester.name;
		request(url, { json: true }, (err, res, body) => {
			if (err) { 
				return console.log(err);
			}
			
			this.setState({tests: body})

			var rows = <React.Fragment>
				{this.state.tests.map((test, i) => {
					const modelID = test.model_id_2 ? test.model_id_2 : test.model_id_1;
					if (!modelID || !this.state.modelDict[modelID]) {
						return <tr>
							<th scope="row">{i + 1}</th>
							<td>
								<Link to={location => {
								var idx = location.pathname.lastIndexOf("/");
								var newPath = location.pathname.substring(0, idx);
								idx = location.pathname.lastIndexOf("/");
								newPath = newPath.substring(0, idx);
								location.pathname = newPath;
								return `/crashtest/${test.id}`
								}}>{test.num}</Link>
							</td>
							<td>{test.test_type}</td>
							<td>N/A</td>
							<td>N/A</td>
							<td>N/A</td>
						</tr>
					}
					return <tr>
						<th scope="row">{i + 1}</th>
						<td>
							<Link to={location => {
							var idx = location.pathname.lastIndexOf("/");
							var newPath = location.pathname.substring(0, idx);
							idx = location.pathname.lastIndexOf("/");
							newPath = newPath.substring(0, idx);
							location.pathname = newPath;
							return `/crashtest/${test.id}`
							}}>{test.num}</Link>
						</td>
						<td>{test.test_type}</td>
						<td>{this.state.modelDict[modelID][0]}</td>
						<td>{this.state.modelDict[modelID][1]}</td>
						<td>{this.state.modelDict[modelID][2]}</td>
					</tr>
				})}

			</React.Fragment>

			this.setState({testRows: rows, testsLoading: false, loading: false})
		});	
	}


	render() {
		const tester = Object(this.state.data);
		console.log(this.state)
		return (
			<Router>
				<Switch>
					<Route path="/carbrand/:id" component={CarBrandInst} />
					<Route path="/crashtest/:id" component={CrashTestInst} />
					<Route path="/carmodel/:id" component={CarModelInst} />
					<Route>
						{this.state.loading && <div style={{width: "100%", marginTop: "200px", textAlign: "center"}}><Spinner style={{ width: '6rem', height: '6rem' }} type="grow" /></div>}
						{!this.state.loading && <React.Fragment>
							<div style={{position: "absolute", width: "100%", height: "350px", background: "rgba(0, 0, 0, .5)"}} />
							<Jumbotron style={{backgroundImage: this.state.otherImgURL, height: "350px", backgroundPosition: "center center"}} />
							<div style={{"textAlign": "center"}}>
								<div style={{boxShadow: "5px 5px 15px grey", borderRadius: "50%", position: "absolute", marginTop: "-130px", marginLeft: "-100px", display: "inline-block", height: logoCircleSize, width: logoCircleSize, "text-align": "center", "background": "white"}}>
									{tester.logo ? <img style={{marginTop: logoTopMargin}} src={tester.logo} width={logoWidth} /> : 
									<div style={{marginTop: "90px"}}><h5>{tester.name}</h5></div>}
								</div>
							</div>
							<br/>
							<br/>
							<br/>
							<br/>

							<Container>
								<Row>
									<Col sm="6" style={{marginTop: "30px"}}>
										<h2><b>{tester.name}</b></h2>
										{tester.year_founded ? "Est. " + tester.year_founded : ""}
										<br/>
										{tester.location ? tester.location : ""}
										<br/><br/><br/>
									</Col>
									<Col sm="6" style={{textAlign: "right", marginTop: "30px"}}>
										{tester.website ? <Button color="primary" size="lg" href={tester.website}>Website</Button> : ""}
										{' '}
										{tester.twitter_handle ? <Button color="primary" size="lg" href={"https://twitter.com/" + tester.twitter_handle}><FaTwitter /></Button> : ""}
									</Col>
									{tester.location_count ? <Col md="4" style={{marginTop: "0px"}}>
										Number of Locations: {tester.location_count}
									</Col> : ""}
									{tester.employee_count ? <Col md="4" style={{marginTop: "10px"}}>
										Number of Employees: {tester.employee_count}
									</Col> : ""}
									{tester.test_count ? <Col md="4" style={{marginTop: "10px"}}>
										Number of Crash Tests: {tester.test_count}
									</Col> : ""}
									<div style={{width:"100%"}} class="border-top my-3"></div><br/><br/>
									{tester.map_link ? <Col md="6" style={{marginTop: "30px"}}>
										<iframe src={tester.map_link} width="100%" height="400" frameborder="0" style={{border: "0"}} allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
									</Col> : ""}
									{tester.twitter_handle ?
									<Col md="6" style={{textAlign: "center"}}>
										<TwitterTimelineEmbed
										sourceType="profile"
										screenName={tester.twitter_handle.substring(1)}
										options={{width: "100%", height: 400}}
										/>
									</Col> : ""}
									<Col xl="6" style={{marginTop: "50px", height: "500px", overflow: "scroll"}}>
										<h6 style={{textAlign: "center"}}><b>Car Models Tested</b></h6><br/>
										<Table striped>
											<thead>
												<tr>
												<th>#</th>
												<th>Year</th>
												<th>Make</th>
												<th>Model</th>
												</tr>
											</thead>
											<tbody>
												{!this.state.modelsLoading && this.state.modelRows}
											</tbody>
										</Table>
									</Col>
									<Col xl="6" style={{marginTop: "50px", height: "500px", overflow: "scroll"}}>
										<h6 style={{textAlign: "center"}}><b>Crash Tests Performed</b></h6><br/>
										<Table striped>
											<thead>
												<tr>
												<th>#</th>
												<th>Test No.</th>
												<th>Test Type</th>
												<th>Car Year</th>
												<th>Car Make</th>
												<th>Car Model</th>
												</tr>
											</thead>
											<tbody>
												{!this.state.testsLoading && this.state.testRows}
											</tbody>
										</Table>
									</Col>
								</Row>
								<br/><br/><br/>
							</Container>

						</React.Fragment>}
					</Route>
				</Switch>
			</Router>
		);
	}
}
export default ProviderInst;
