import React from 'react';
import * as d3 from 'd3';
import { Line } from 'react-chartjs-2';

class AvgRatingByType extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data : [],
            xAttr: "year",
            yAttr: "avgR"
        }
    }

    componentDidMount() {
        this.getData();
    }

    async getData() {
        var ratingsByYear = {"datasets": [{
			"label": "Average Safety Rating",
			"backgroundColor": "Blue",
			"borderColor": "Blue",
			"fill": false,
			"data": []
		}]};
		var years = [];
        var rating = {};
        var count = {};

        await d3.json("http://api.crashsafe.me/models").then(response => {
			for(let r in response) {
				let year = response[r].year;
				if(!years.includes(year)) {
					years.push(year);
					rating[year] = 0;
					count[year] = 0;
				}
			}
			years.sort();
			for(let r in response) {
                let model = response[r];
                let modelAvgRating = this.calculateModelsSafetyRating(model);
				rating[model.year] += modelAvgRating;
				count[model.year] += 1;
			}
			console.log(rating);
		});
        
		for(let i in years) {
			let year = years[i];
			ratingsByYear.datasets[0].data.push({"x": year, "y": rating[year]/count[year]});
		}
		ratingsByYear.labels = years;
        this.setState({data: ratingsByYear});
    }

    calculateModelsSafetyRating(model) {
        let IIHS_ratings = [this.getIIHSRating(model.IIHS_side),
                            this.getIIHSRating(model.IIHS_frontModerateOverlap),
                            this.getIIHSRating(model.IIHS_frontSmallOverlap),
                            this.getIIHSRating(model.IIHS_rollover),
                            this.getIIHSRating(model.IIHS_rear)]
        let IIHS_rating = this.calculateModelAvgRating(IIHS_ratings)
        
        let NHTSA_ratings = [this.getNHTSARating(model.NHTSA_overall_rating),
                            this.getNHTSARating(model.NHTSA_overall_side_crash_rating),
                            this.getNHTSARating(model.NHTSA_overall_front_crash_rating),
                            this.getNHTSARating(model.NHTSA_rollover_rating)]
        let NHTSA_rating = this.calculateModelAvgRating(NHTSA_ratings)

        var num = 0
        num += IIHS_rating > 0 ? 1 : 0
        num += NHTSA_rating > 0 ? 1 : 0
        return num > 0 ? (IIHS_rating + NHTSA_rating) * 1.0 / num : 0.0
    }

    calculateModelAvgRating(ratings) {
        var total = 0
        var num = 0
        for (let i in ratings) {
            total += ratings[i]
            num += ratings[i] == 0 ? 0 : 1
        }
        return num > 0 ? total * 1.0 / num : 0.0
    }

    getIIHSRating(ratingStr) {
        switch (ratingStr) {
            case "N/A":
                return 0.0
            case "Good":
                return 5.0
            case "Acceptable":
                return 4.0
            case "Marginal":
                return 3.0
            case "Poor":
                return 2.0
            default:
                return 0.0
        }
    }

    getNHTSARating(rating) {
        switch(rating) {
            case null:
                return 0.0
            default:
                return parseInt(rating)
        }
        return 1.0
    }

    render() {
		return <Line data={this.state.data}/>
    }
}

export default AvgRatingByType;
