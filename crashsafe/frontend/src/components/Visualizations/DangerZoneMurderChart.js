import React from 'react';
import * as d3 from 'd3';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import './map.css';


class DangerZoneMurderChart extends React.Component {
    constructor() {
      super()
      this.state = {
        width: 0,
        height: 0,
        plotData: [],
      }
      this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        this.getData();
    }

    async getData() {
        let url = "https://api.dangerzone.life/crime";
        let data = [];
		await d3.json(url)
			.then(res => {
				for(let crime in res){
					let entry = {};
					if(res[crime].city && res[crime].city !== "")
					{
                        let cityName = res[crime].city;
                        let murderVal = res[crime].murder;
                        murderVal = Math.pow(murderVal, 4);
                        entry["label"] = cityName;
                        entry["value"] = murderVal;
						data.push(entry);
					}
				}
			});
        this.setState({plotData : data});
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        console.log(this.state.plotData);
        return(
            <React.Fragment>
                <BubbleChart
                graph= {{
                    zoom: 1.0,
                }}
                width={this.state.width - 400}
                height={this.state.width - 400}
                padding={0} // optional value, number that set the padding between bubbles
                showLegend={false}
                legendFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#000',
                        weight: 'bold',
                    }}
                valueFont={{
                        family: 'Arial',
                        size: 0,
                        color: '#fff',
                        weight: 'bold',
                    }}
                labelFont={{
                        family: 'Arial',
                        size: 12,
                        color: '#fff',
                        weight: 'bold',
                    }}
                data={this.state.plotData}
                />
            </React.Fragment>
        );
    }

}

export default DangerZoneMurderChart;
