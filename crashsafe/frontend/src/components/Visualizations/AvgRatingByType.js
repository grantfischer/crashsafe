import React from 'react';
import * as d3 from 'd3';

class AvgRatingByType extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data : [],
            xAttr: "type",
            yAttr: "avgR"
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentDidUpdate() {
        this.createBarChart();
    }

    async getData() {
        var ratingsByType = [{"type": "PICKUP TRUCK", "avgR": 0, "num": 0},
                                {"type": "TWO DOOR COUPE", "avgR": 0, "num": 0},
                                {"type": "THREE DOOR COUPE", "avgR": 0, "num": 0},
                                {"type": "THREE DOOR HATCHBACK", "avgR": 0, "num": 0},
                                {"type": "FIVE DOOR HATCHBACK", "avgR": 0, "num": 0},
                                {"type": "LIMOUSINE", "avgR": 0, "num": 0},
                                {"type": "MOTOR HOME", "avgR": 0, "num": 0},
                                {"type": "BUS", "avgR": 0, "num": 0},
                                {"type": "STATION WAGON", "avgR": 0, "num": 0},
                                {"type": "CONVERTIBLE", "avgR": 0, "num": 0},
                                {"type": "SUV", "avgR": 0, "num": 0},
                                {"type": "VAN", "avgR": 0, "num": 0},
                                {"type": "MINIVAN", "avgR": 0, "num": 0},
                                {"type": "TWO DOOR SEDAN", "avgR": 0, "num": 0},
                                {"type": "FOUR DOOR SEDAN", "avgR": 0, "num": 0},
                                {"type": "TRUCK", "avgR": 0, "num": 0}];
        
        await d3.json("https://api.crashsafe.me/models")
            .then(response => {
                for (let i in response) {
                    let model = response[i]
                    let modelAvgRating = this.calculateModelsSafetyRating(model)
                    if (modelAvgRating > 0) {
                        switch (model.vehicle_type) {
                            case ratingsByType[0].type:
                                ratingsByType[0].avgR += modelAvgRating
                                ratingsByType[0].num += 1
                                break
                            case ratingsByType[1].type:
                                ratingsByType[1].avgR += modelAvgRating
                                ratingsByType[1].num += 1
                                break
                            case ratingsByType[2].type:
                                ratingsByType[2].avgR += modelAvgRating
                                ratingsByType[2].num += 1
                                break
                            case ratingsByType[3].type:
                                ratingsByType[3].avgR += modelAvgRating
                                ratingsByType[3].num += 1
                                break
                            case ratingsByType[4].type:
                                ratingsByType[4].avgR += modelAvgRating
                                ratingsByType[4].num += 1
                                break
                            case ratingsByType[5].type:
                                ratingsByType[5].avgR += modelAvgRating
                                ratingsByType[5].num += 1
                                break
                            case ratingsByType[6].type:
                                ratingsByType[6].avgR += modelAvgRating
                                ratingsByType[6].num += 1
                                break
                            case ratingsByType[7].type:
                                ratingsByType[7].avgR += modelAvgRating
                                ratingsByType[7].num += 1
                                break
                            case ratingsByType[8].type:
                                ratingsByType[8].avgR += modelAvgRating
                                ratingsByType[8].num += 1
                                break
                            case ratingsByType[9].type:
                                ratingsByType[9].avgR += modelAvgRating
                                ratingsByType[9].num += 1
                                break
                            case ratingsByType[10].type:
                                ratingsByType[10].avgR += modelAvgRating
                                ratingsByType[10].num += 1
                                break
                            case ratingsByType[11].type:
                                ratingsByType[11].avgR += modelAvgRating
                                ratingsByType[11].num += 1
                                break
                            case ratingsByType[12].type:
                                ratingsByType[12].avgR += modelAvgRating
                                ratingsByType[12].num += 1
                                break
                            case ratingsByType[13].type:
                                ratingsByType[13].avgR += modelAvgRating
                                ratingsByType[13].num += 1
                                break
                            case ratingsByType[14].type:
                                ratingsByType[14].avgR += modelAvgRating
                                ratingsByType[14].num += 1
                                break
                            case ratingsByType[15].type:
                                ratingsByType[15].avgR += modelAvgRating
                                ratingsByType[15].num += 1
                                break
                        }
                    }

                }
            });
        for (let i in ratingsByType) {
            if (ratingsByType[i].avgR > 0) {
                let avgR = ratingsByType[i].avgR  * 1.0 / ratingsByType[i].num
                ratingsByType[i].avgR = Math.round((avgR + Number.EPSILON) * 100) / 100
            }
        }
        console.log(ratingsByType)
        this.setState({data: ratingsByType});
    }

    calculateModelsSafetyRating(model) {
        let IIHS_ratings = [this.getIIHSRating(model.IIHS_side),
                            this.getIIHSRating(model.IIHS_frontModerateOverlap),
                            this.getIIHSRating(model.IIHS_frontSmallOverlap),
                            this.getIIHSRating(model.IIHS_rollover),
                            this.getIIHSRating(model.IIHS_rear)]
        let IIHS_rating = this.calculateModelAvgRating(IIHS_ratings)
        
        let NHTSA_ratings = [this.getNHTSARating(model.NHTSA_overall_rating),
                            this.getNHTSARating(model.NHTSA_overall_side_crash_rating),
                            this.getNHTSARating(model.NHTSA_overall_front_crash_rating),
                            this.getNHTSARating(model.NHTSA_rollover_rating)]
        let NHTSA_rating = this.calculateModelAvgRating(NHTSA_ratings)

        var num = 0
        num += IIHS_rating > 0 ? 1 : 0
        num += NHTSA_rating > 0 ? 1 : 0
        return num > 0 ? (IIHS_rating + NHTSA_rating) * 1.0 / num : 0.0
    }

    calculateModelAvgRating(ratings) {
        var total = 0
        var num = 0
        for (let i in ratings) {
            total += ratings[i]
            num += ratings[i] == 0 ? 0 : 1
        }
        return num > 0 ? total * 1.0 / num : 0.0
    }

    getIIHSRating(ratingStr) {
        switch (ratingStr) {
            case "N/A":
                return 0.0
            case "Good":
                return 5.0
            case "Acceptable":
                return 4.0
            case "Marginal":
                return 3.0
            case "Poor":
                return 2.0
            default:
                return 0.0
        }
    }

    getNHTSARating(rating) {
        switch(rating) {
            case null:
                return 0.0
            default:
                return parseInt(rating)
        }
        return 1.0
    }

    createBarChart() {
        const margin = { top: 50, right: 20, bottom: 100, left: 60 };
        const height = 600 - margin.top - margin.bottom;
        const width = 1200 - margin.left - margin.right;

        const svg = d3
            .select(this.refs.barChart)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        const x = d3
            .scaleBand()
            .range([0, width])
            .domain(this.state.data.map(d => d[this.state.xAttr]))
            .padding(0.2);
        svg.append('g')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.axisBottom(x))
            .selectAll('text')
            .attr('transform', 'translate(-10,0)rotate(-45)')
            .style('text-anchor', 'end');

        const maxValue = Math.max(...this.state.data.map(d => d[this.state.yAttr]));
        const y = d3
            .scaleLinear()
            .domain([0, Math.ceil(maxValue / 5) * 5])
            .range([height, 0]);
        svg.append('g').call(d3.axisLeft(y));

        svg.selectAll('mybar')
            .data(this.state.data)
            .enter()
            .append('rect')
            .attr('x', d => x(d[this.state.xAttr]))
            .attr('y', d => y(d[this.state.yAttr]))
            .attr('width', x.bandwidth())
            .attr('height', d => height - y(d[this.state.yAttr]))
            .attr('fill', '#2bc4ad')
    }

    render() {
        return <div ref="barChart"></div>;
    }
}

export default AvgRatingByType;
