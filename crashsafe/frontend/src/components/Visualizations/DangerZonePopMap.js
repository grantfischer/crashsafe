import React from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson-client';
import './map.css';


class DangerZonePopMap extends React.Component {
    constructor() {
      super()
      this.state = {
        usData: null,
        usCongress: null,
        usCities: null
      }
    }

    componentWillMount() {
        Promise.all([
          d3.json('https://raw.githubusercontent.com/Swizec/113th-congressional-districts/master/public/us.json'),
          d3.json('https://raw.githubusercontent.com/Swizec/113th-congressional-districts/master/public/us-congress-113.json'),
          d3.json('https://api.dangerzone.life/cities')
          ]).then( ([usData, usCongress, usCities, usCrime]) => {
              this.setState({
                  usData,
                  usCongress,
                  usCities,
                  usCrime
              });
          }).catch(err => console.log('Error loading or parsing data.'))
    }

    componentDidUpdate() {
        const svg = d3.select(this.refs.anchor),
              { width, height } = this.props;

        const projection = d3.geoAlbers()
                             .scale(1280)
                             .translate([width / 2, height / 2]);

        const path = d3.geoPath(projection);

        const us = this.state.usData,
              congress = this.state.usCongress,
              usCities = this.state.usCities;
        var max_pop = Math.max.apply(Math, usCities.map(function(x) { return x.population }))
        var min_pop = Math.min.apply(Math, usCities.map(function(x) { return x.population }))
        console.log(max_pop, min_pop, usCities)

        svg.append("defs").append("path")
           .attr("id", "land")
           .datum(topojson.feature(us, us.objects.land))
           .attr("d", path);
 
          svg.append("clipPath")
             .attr("id", "clip-land")
             .append("use")
             .attr("xlink:href", "#land");
 
        svg.append("g")
           .attr("class", "districts")
           .attr("clip-path", "url(#clip-land)")
           .selectAll("path")
           .data(topojson.feature(congress, congress.objects.districts).features)
           .enter().append("path")
           .attr("d", path)

         svg.append("path")
            .attr("class", "district-boundaries")
            .datum(topojson.mesh(congress, congress.objects.districts, function(a, b) { return a !== b && (a.id / 1000 | 0) === (b.id / 1000 | 0); }))
            .attr("d", path);
 
        svg.append("path")
           .attr("class", "state-boundaries")
           .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
           .attr("d", path);
 
        svg.append("g")
           .attr("class", "cities")
           .selectAll("circle")
           .data(usCities)
           .enter()
           .append("circle")
              .attr("cx", function(d) { return projection([d.longitude, d.latitude])[0] })
              .attr("cy", function(d) { return projection([d.longitude, d.latitude])[1] })
              .attr("r",  function(d) { return 100 * ((d.population - min_pop) / (max_pop - min_pop)) })
              .style("fill", "69b3a2")
              .attr("stroke", "#69b3a2")
              .attr("stroke-width", 3)
              .attr("fill-opacity", .2)
              .append("title")
              .text(function(d) {return d.city + ", " + d.population})
    }

    render() {
        const { usData, usCongress, usCities } = this.state;

        if (!usData || !usCongress || !usCities ) {
            return null;
        }

        return <g ref="anchor" />;
    }
}

export default DangerZonePopMap