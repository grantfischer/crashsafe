import React from 'react';
import {
	Col,
	Row,
	ListGroup,
	ListGroupItem,
	Table,
	Button,
	Spinner
} from 'reactstrap';
import styled from 'styled-components';
import * as d3 from "d3";
import {Element} from "react-faux-dom";

const request = require('request');

class AirPollution extends React.Component {

	state = {
		plot : []
	};

	async getPlot() {
		const data = [];
		await d3.json("https://api.dangerzone.life/health/filter?air_pollution_particle_gt=-1")
			.then(res => {
				for(let city in res){
					let copy = {};
					if(res[city].life_expectancy > -1)
					{
						copy.city = res[city].city;
						copy.air_pollution_particle = res[city].air_pollution_particle;
						copy.life_expectancy = res[city].life_expectancy;
						data.push(copy);
					}
				}
			});
		this.setState({plot : data});
	}

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.getPlot()
	}

	render() {
		if(this.state.plot.length == 0){
			return <b> Loading... </b>
		}
		else {
			let data = this.state.plot;
			let margin = ({top: 25, right: 20, bottom: 35, left: 40});
			let height = 600;
            let width = 960;

			let x = d3.scaleLinear()
				.domain(d3.extent(data, d => d.air_pollution_particle))
				.nice().range([margin.left, width - margin.right]);

			let y = d3.scaleLinear()
				.domain(d3.extent(data, d => d.life_expectancy))
				.nice().range([height - margin.bottom, margin.top]);

			let xAxis = g => g
				.attr("transform", `translate(0, ${height - margin.bottom})`)
				.call(d3.axisBottom(x).ticks(width/80))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", width)
					.attr("y", margin.bottom - 4)
					.attr("fill", "currentColor")
					.attr("text-anchor", "end")
					.text("Air Pollution Particles per Cubic Meter"));

			let yAxis = g => g
				.attr("transform", `translate(${margin.left}, 0)`)
				.call(d3.axisLeft(y))
				.call(g => g.select(".domain").remove())
				.call(g => g.append("text")
					.attr("x", -margin.left)
					.attr("y", 10)
					.attr("fill", "currentColor")
					.attr("text-anchor", "start")
					.text("Average Life Expectancy in Years"));

			let grid = g => g
				.attr("stroke", "currentColor")
				.attr("stroke-opacity", 0.1)
				.call(g => g.append("g")
					.selectAll("line")
					.data(x.ticks())
					.join("line")
						.attr("x1", d => 0.5 + x(d))
						.attr("x2", d => 0.5 + x(d))
						.attr("y1", margin.top)
						.attr("y2", height - margin.bottom))
				.call(g => g.append("g")
					.selectAll("line")
					.data(y.ticks())
					.join("line")
						.attr("y1", d => 0.5 + y(d))
						.attr("y2", d => 0.5 + y(d))
						.attr("x1", margin.left)
						.attr("x2", width - margin.right));

			let chart = () => {
				const el = new Element("div");
				const svg = d3.select(el)
					.append("svg")
					.attr("id", "chart")
					.attr("width", width)
					.attr("height", height);

				svg.append("g").call(xAxis);
				svg.append("g").call(yAxis);
				svg.append("g").call(grid);

				svg.append("g")
						.attr("stroke", "steelblue")
						.attr("stroke-width", 1.5)
						.attr("fill", "none")
					.selectAll("circle")
					.data(data)
					.join("circle")
						.attr("cx", d => x(d.air_pollution_particle))
						.attr("cy", d => y(d.life_expectancy))
						.attr("r", 3);

				return el.toReact();
			};

			return <div width="960" height="600">{chart()}</div>
		}
	}
} export default AirPollution;
