import React, { useState } from 'react';

import {withRouter, BrowserRouter, Redirect } from 'react-router-dom';
import {
	Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    FormGroup,
    Input,
    Button,
    Form,
    Row

} from 'reactstrap';

class Header extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        query: "",
        isOpen: false,
      }
    }

    handleChange = (event) => {
      this.setState({query: event.target.value});
    }

    handleSubmit = () => {
      this.props.history.push('/search');
    }

    toggle = () => {
      this.setState({isOpen: !this.state.isOpen});
    }

    render() {
      return (
        <div >
          <Navbar light expand="md" style={{"background": "#0c2340"}}>
            <NavbarBrand href="/" style={{"color":"white"}}><b>CrashSafe</b></NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="mr-auto" navbar>
                <NavItem>
                  <NavLink href="/" style={{"color":"white"}}>Home</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/carbrand" style={{"color":"white"}}> Car Brand </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/carmodel" style={{"color":"white"}}> Car Model </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/crashtest" style={{"color":"white"}}> Crash Test </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/provider" style={{"color":"white"}}> Test Provider </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/visualizations" style={{"color":"white"}}> Visualizations </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/about" style={{"color":"white"}}>About</NavLink>
                </NavItem>
              </Nav>
              {/* <Row style={{marginRight: "30px"}}>
                <Form onSubmit={this.handleSubmit}>
                  <Input type="text" name="search" id="searchbar" placeholder="Search Site" onChange={this.handleChange} />
                  <Redirect to='/search' />;
                </Form>
              </Row> */}
            </Collapse>
          </Navbar>
        </div>
      );
    }
  }
export default Header;
