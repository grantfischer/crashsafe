import React from "react";
import "./Background.css";

const Background = ({ src }) => {
  return (
    <div className="Background">
        <div className="Background-image-mask" />
        <img alt="background" src={src} style={{width: "100%"}} />
    </div>
  );
};

export default Background;
