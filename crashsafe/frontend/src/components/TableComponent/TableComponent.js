import React from 'react'

import CssBaseline from '@material-ui/core/CssBaseline'
import EnhancedTable from './EnhancedTable'
import getData from './getData'
import matchSorter from 'match-sorter'
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Backdrop from '@material-ui/core/Backdrop';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';

import CompareComponent from './../CompareComponent/CompareComponent';

import { makeStyles } from '@material-ui/core/styles';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  appBar: {
    position: 'relative',
    background: '#1b2841',
    width: '101%'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

function getCols(table) {
  switch(table) {
    case "brands":
      return [
        {
          Header: "Name",
          accessor: "name",
          isWebsite: false,
          isLink: true,
          isImg: false,
        },
        {
          Header: 'Year Founded',
          accessor: 'year_founded',
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: 'Model Count',
          accessor: 'model_count',
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: 'Safety Rating',
          accessor: 'safety_rating',
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: 'Website',
          accessor: 'website',
          isWebsite: true,
          isLink: false,
          isImg: false,
        },
        {
          Header: 'Logo',
          accessor: 'logo',
          isWebsite: false,
          isLink: false,
          isImg: true,
        },
        {
          Header: "User Rating",
          accessor: "user_rating",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
      ];
    case "models":
      return [
        {
          accessor: "selection",
          Filter: NoFilter
        },
        {
          Header: "Year", 
          accessor: "year",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Make", 
          accessor: "make",
          isWebsite: false,
          isLink: true,
          isImg: false,
          isMake: true,
        },
        {
          Header: "Model", 
          accessor: "model",
          isWebsite: false,
          isLink: true,
          isImg: false,
        },
        {
          Header: "Engine Type", 
          accessor: "engine_type",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },  
        {
          Header: "HP", 
          accessor: "horse_power",
          Filter: HPRangeColumnFilter,
          filter: hpFilter,
          isWebsite: false,
          isLink: false,
          isImg: false,
        },  
        {
          Header: "Vehicle Type", 
          accessor: "vehicle_type",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },  
        {
          Header: "IIHS_frontModerateOverlap", 
          accessor: "IIHS_frontModerateOverlap", 
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "IIHS_frontSmallOverlap", 
          accessor: "IIHS_frontSmallOverlap", 
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "IIHS_side", 
          accessor: "IIHS_side", 
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "IIHS_rollover", 
          accessor: "IIHS_rollover", 
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "IIHS_rear", 
          accessor: "IIHS_rear", 
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "NHTSA_overall_rating", 
          accessor: "NHTSA_overall_rating", 
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "NHTSA_overall_side_crash_rating", 
          accessor: "NHTSA_overall_side_crash_rating", 
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "NHTSA_overall_front_crash_rating", 
          accessor: "NHTSA_overall_front_crash_rating", 
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "NHTSA_rollover_rating", 
          accessor: "NHTSA_rollover_rating",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        }
      ];
    case "tests":
      return [
        {
          accessor: "selection",
          Filter: NoFilter
        },
        {
          Header: "Num",
          accessor: "num",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: true,
          isImg: false,
        },
        {
          Header: "Test Type",
          accessor: "test_type",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Model ID", 
          accessor: "model_id_1",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Model ID 2", 
          accessor: "model_id_2",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Test Performer", 
          accessor: "test_performer",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Impact Angle", 
          accessor: "impact_angle",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Max Crush", 
          accessor: "max_crush",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Collision Speed", 
          accessor: "collision_speed",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Max Crush 2", 
          accessor: "max_crush_2",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Collision Speed 2", 
          accessor: "collision_speed_2",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        }
      ];
    case "providers":
      return [
        {
          Header: "Name", 
          accessor: "name",
          isWebsite: false,
          isLink: true,
          isImg: false,
        },
        {
          Header: "Year Founded", 
          accessor: "year_founded",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Location", 
          accessor: "location",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Num Tests", 
          accessor: "test_count",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Num Employees", 
          accessor: "employee_count",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Num Locations", 
          accessor: "location_count",
          Filter: NumberRangeColumnFilter,
          filter: "between",
          isWebsite: false,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Website", 
          accessor: "website",
          isWebsite: true,
          isLink: false,
          isImg: false,
        },
        {
          Header: "Logo", 
          accessor: "logo",
          isWebsite: false,
          isLink: false,
          isImg: true,
        }
      ];
  }
}

// --------------------------------------------------------------------------------------------------------------
// ------------------------------- FILTERS ----------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------------------

function NoFilter(){
  return(
    <div></div>
  );
};


// This is a custom UI for our 'between' or number range
// filter. It uses two number boxes and filters rows to
// ones that have values between the two
function NumberRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}) {
  const [min, max] = React.useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    preFilteredRows.forEach(row => {
      min = Math.min(row.values[id], min)
      max = Math.max(row.values[id], max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <input
        value={filterValue[0] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [val ? parseInt(val, 10) : undefined, old[1]])
        }}
        placeholder={`Min (${min})`}
        style={{
          width: '70px',
          marginRight: '0.5rem',
        }}
      />
      to
      <input
        value={filterValue[1] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [old[0], val ? parseInt(val, 10) : undefined])
        }}
        placeholder={`Max (${max})`}
        style={{
          width: '70px',
          marginLeft: '0.5rem',
        }}
      />
    </div>
  )
}

// Filter for HP column
function HPRangeColumnFilter({
  column: { filterValue = [], preFilteredRows, setFilter, id },
}) {
  const [min, max] = React.useMemo(() => {
    let min = preFilteredRows.length ? convertToInt(preFilteredRows[0].values[id]) : 0
    let max = preFilteredRows.length ? convertToInt(preFilteredRows[0].values[id]) : 0
    
    preFilteredRows.forEach(row => {
      min = Math.min(convertToInt(row.values[id]), min)
      max = Math.max(convertToInt(row.values[id]), max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <input
        value={filterValue[0] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          //setFilter((old = []) => [val ? parseInt(val, 10) : undefined, old[1]])
          setFilter((old = []) => [val ? parseInt(val, 10) : undefined, old[1]])
        }}
        placeholder={`Min (${min})`}
        style={{
          width: '70px',
          marginRight: '0.5rem',
        }}
      />
      to
      <input
        value={filterValue[1] || ''}
        type="number"
        onChange={e => {
          const val = e.target.value
          setFilter((old = []) => [old[0], val ? parseInt(val, 10) : undefined])
        }}
        placeholder={`Max (${max})`}
        style={{
          width: '70px',
          marginLeft: '0.5rem',
        }}
      />
    </div>
  )
}

function convertToInt(rowValue) {
  if (typeof rowValue === 'string' || rowValue instanceof String) {
    let list = rowValue.split(" ")
    return parseInt(list[0], 10)
  } else {
    return null
  }
}

function hpFilter(rows, id, filterValue) {
  // console.log(filterValue)
  return rows.filter(row => {
    const rowValue = convertToInt(row.values[id])
    let min = filterValue[0] ? filterValue[0] : 0
    let max = filterValue[1] ? filterValue[1] : 470
    return rowValue >= min && rowValue <= max
  })
}

// This is a custom filter UI that uses a
// slider to set the filter value between a column's
// min and max values
function SliderColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {
  // Calculate the min and max
  // using the preFilteredRows

  const [min, max] = React.useMemo(() => {
    let min = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    let max = preFilteredRows.length ? preFilteredRows[0].values[id] : 0
    preFilteredRows.forEach(row => {
      min = Math.min(row.values[id], min)
      max = Math.max(row.values[id], max)
    })
    return [min, max]
  }, [id, preFilteredRows])

  return (
    <div>
      <input
        type="range"
        min={min}
        max={max}
        value={filterValue || min}
        onChange={e => {
          setFilter(parseInt(e.target.value, 10))
        }}
      />
      <button onClick={() => setFilter(undefined)}>Off</button>
    </div>
  )
}

// This is a custom filter UI for selecting
// a unique option from a list
function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = React.useMemo(() => {
    const options = new Set()
    preFilteredRows.forEach(row => {
      options.add(row.values[id])
    })
    return [...options.values()]
  }, [id, preFilteredRows])

  // Render a multi-select box
  return (
    <select
      value={filterValue}
      onChange={e => {
        setFilter(e.target.value || undefined)
      }}
    >
      <option value="">All</option>
      {options.map((option, i) => (
        <option key={i} value={option}>
          {option}
        </option>
      ))}
    </select>
  )
}

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = val => !val

const TableComponent = (props) => {
  const classes = useStyles();

  const {table} = props;
  const columns = getCols(table);
  const [data, setData] = React.useState(getData(props.num, props.tableData));
  const [compareData, setCompareData] = React.useState([]);
  const [skipPageReset, setSkipPageReset] = React.useState(false);

  // Compare
  const [compare, setCompare] = React.useState(false);
  const setShowCompare = (compareData) => {
    setCompare(true);
    setCompareData(compareData);
    // console.log(compareData);
  }
  const [compareError, setCompareError] = React.useState(false);

  return (
    <div>
      <Snackbar
      open={compareError} 
      autoHideDuration={5000} 
      onClose={() => setCompareError(false)}>
          <MuiAlert elevation={6} variant="filled" severity="error">
              Can't Compare More Than 6 Items
          </MuiAlert>
      </Snackbar>

      {compare && <Backdrop className={classes.backdrop} open={compare}>
        <Button>Close</Button>
        {/* <div style={{width: "100%", height: "100%", background: "yellow"}}> */}
          <Dialog fullScreen open={compare} onClose={() => setCompare(false)} TransitionComponent={Transition}>
            <div style={{width: "99%"}}>
            <AppBar className={classes.appBar}>
              <Toolbar>
                <IconButton edge="start" color="inherit" onClick={() => setCompare(false)} aria-label="close">
                  <CloseIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  {table === "models" && `Compare Car Models`}
                  {table === "tests" && `Compare Crash Tests`}
                </Typography>
              </Toolbar>
            </AppBar>
            <CompareComponent data={compareData} table={table} />
            </div>
          </Dialog>
        {/* </div> */}
      </Backdrop>}

      <CssBaseline />
      <EnhancedTable
        tableType={table}
        columns={columns}
        data={data}
        setData={setData}
        setShowCompare={setShowCompare}
        setCompareError={setCompareError}
      />
    </div>
  )
}

export default TableComponent
