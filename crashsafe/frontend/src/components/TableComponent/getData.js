const fetch = require('node-fetch');

const range = len => {
  const arr = []
  for (let i = 0; i < len; i++) {
    arr.push(i)
  }
  return arr
}

export default function makeData(numToGet, data) {
  const getData = () => {
    return range(numToGet).map(idx => {
      return {
        ...data[idx],
      }
    })
  }
  return getData();
}
