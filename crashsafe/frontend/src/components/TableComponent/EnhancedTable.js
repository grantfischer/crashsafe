import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";

import Highlighter from "react-highlight-words"

import MaUTable from '@material-ui/core/Table'
import PropTypes from 'prop-types'
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableFooter from '@material-ui/core/TableFooter'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TablePaginationActions from './TablePaginationActions'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import TableToolbar from './TableToolbar'
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
  useFilters,
} from 'react-table'
import {
  Row,
  Col,
} from 'reactstrap';

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Define a default UI for filtering
function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length

  return (
    <input
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  )
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------


// Set our default
const defaultColumn = {
  Filter: DefaultColumnFilter,
}


const EnhancedTable = ({
  tableType,
  columns,
  data,
  setData,
  skipPageReset,
  setShowCompare,
  setCompareError,
}) => {
  const {
    getTableProps,
    headerGroups,
    prepareRow,
    page,
    gotoPage,
    setPageSize,
    preGlobalFilteredRows,
    setGlobalFilter,
    state: { pageIndex, pageSize, globalFilter },
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      tableType,
      autoResetPage: !skipPageReset,
    },
    useGlobalFilter,
    useFilters,
    useSortBy,
    usePagination,
  )

  // Compare
  const [selected, setSelected] = React.useState([]);
  const [compareData, setCompareData] = React.useState([]);
  const clearSelected = () => {
    setSelected([]);
    setCompareData([]);
  }
  const isSelected = (name) => selected.indexOf(name) !== -1;
  const handleClick = (event, item) => {
    const selectedIndex = selected.indexOf(item.id);

    let newSelected = [];
    let newCompareData = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, item.id);
      newCompareData = newCompareData.concat(compareData, item);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      newCompareData = newCompareData.concat(compareData.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      newCompareData = newCompareData.concat(compareData.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      newCompareData = newCompareData.concat(
        compareData.slice(0, selectedIndex),
        compareData.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
    setCompareData(newCompareData);
  };

  const handleChangePage = (event, newPage) => {
    gotoPage(newPage)
  }

  const handleChangeRowsPerPage = event => {
    setPageSize(Number(event.target.value))
  }

  const getHighlightedText = (text) => {
    return (<Highlighter
              highlightClassName="highlightText"
              searchWords={(globalFilter == undefined) ? [''] : globalFilter.split(' ')}
              autoEscape={true}
              textToHighlight={text}
            />)
  }

  // Render the UI for your table
  return (
    <TableContainer >
      <Row>
        <Col md="9">
          <TableToolbar
            preGlobalFilteredRows={preGlobalFilteredRows}
            setGlobalFilter={setGlobalFilter}
            globalFilter={globalFilter}
            tableType={tableType}
          />
        </Col>
        <Col md="3">
          <Button 
          onClick={() => setShowCompare(compareData)}
          disabled={selected.length <= 1 || selected.length > 6} 
          variant="contained" 
          color="default">
            Compare
          </Button>
          {' '}
          <Button 
          onClick={clearSelected} 
          disabled={selected.length === 0} 
          variant="contained" 
          color="default">
            Clear {selected.length > 0 && selected.length}
          </Button>
        </Col>
      </Row>
      <MaUTable {...getTableProps()}>
        <TableHead>
          {headerGroups.map(headerGroup => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <TableCell
                  {...(column.id === 'selection'
                    ? column.getHeaderProps()
                    : column.getHeaderProps(column.getSortByToggleProps()))}
                >
                  {column.render('Header')}
                  {column.id !== 'selection' ? (
                    <TableSortLabel
                      active={column.isSorted}
                      // react-table has a unsorted state which is not treated here
                      direction={column.isSortedDesc ? 'desc' : 'asc'}
                    />
                  ) : null}
                  <div onClick="#">{column.canFilter ? column.render('Filter') : null}</div>
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>

        <TableBody>
          {page.map((row, i) => {
            prepareRow(row)
            return (
              <TableRow hover onClick={(event) => handleClick(event, row.original)} {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <TableCell {...cell.getCellProps()}>
                        {cell.column.id === 'selection' ? (
                          <Checkbox
                            checked={isSelected(row.original.id)}
                          />
                        ) : null}
                      {cell.column.isLink && cell.column.isMake && <Link to={location => `carbrand/${cell.row.original.brand_id}`}>{cell.render('Cell')}</Link>}
                      {cell.column.isLink && !cell.column.isMake && <Link to={location => `${location.pathname}/${cell.row.original.id}`}>{cell.render('Cell')}</Link>}
                      {!cell.column.isLink && cell.column.isWebsite && <a href={cell.row.original.website}>{cell.row.original.website}</a>}
                      {!cell.column.isWebsite && !cell.column.isLink && cell.column.isImg && <img src={cell.row.original.logo} width="50px" height="50px"/>}
                      {!cell.column.isWebsite && !cell.column.isLink && !cell.column.isImg && !cell.column.isMake && cell.render('Cell')}
                    </TableCell>
                  )
                })}
              </TableRow>
            )
          })}
        </TableBody>

        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[
                5,
                10,
                25,
                { label: 'All', value: data.length },
              ]}
              colSpan={4}
              style={{position: "absolute"}}
              count={data.length}
              rowsPerPage={pageSize}
              page={pageIndex}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
          <br/>
          <TableRow style={{position: "absolute", right: "100px"}}>
            Page {pageIndex + 1} of {Math.ceil(data.length / pageSize)}
          </TableRow>
          <br/>
          <br/>
        </TableFooter>
      </MaUTable>
    </TableContainer>
  )
}

// Define a custom filter filter function!
function filterGreaterThan(rows, id, filterValue) {
  return rows.filter(row => {
    const rowValue = row.values[id]
    return rowValue >= filterValue
  })
}

// This is an autoRemove method on the filter function that
// when given the new filter value and returns true, the filter
// will be automatically removed. Normally this is just an undefined
// check, but here, we want to remove the filter if it's not a number
filterGreaterThan.autoRemove = val => typeof val !== 'number'

EnhancedTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  setData: PropTypes.func.isRequired,
  skipPageReset: PropTypes.bool.isRequired,
}

export default EnhancedTable
