import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Col,
  Row,
  ListGroup,
  ListGroupItem,
  Table,
  Spinner,
} from 'reactstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import styled from 'styled-components';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Rating } from '@material-ui/lab';

import { FaTwitter, FaPlus } from 'react-icons/fa';


class CompareItem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            tableType: props.tableType,
            entryData: props.entryData,
        }
    }

    componentDidMount() {
    }

    render(){
        const obj = Object(this.state.entryData);
        return(
            <Col>
                {this.state.tableType === "brands" && <div style={{width: "250px", height: "410px", margin:"4px"}}>
                    <Card style={{height: "100%"}}>
                        <Link to={location => `${location.pathname}/${this.state.entryData.id}`}>
                            <CardActionArea style={{textAlign: "center"}}>
                                <img
                                src={obj.logo}
                                style={{width: "40%", height: "100%"}}
                                />
                                <CardContent>
                                <Typography gutterBottom variant="h4" component="h2">
                                    {obj.name}
                                </Typography>
                                <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                    <br/>
                                    {obj.year_founded && "Est. "}
                                    {obj.year_founded && obj.year_founded.toString()}
                                    {obj.year_founded && <br/>}
                                    
                                    {'Number of Models: '}
                                    {obj.model_count && obj.model_count.toString()}
                                    {obj.model_count && <br/>}
                                    <br/>
                                    
                                    {obj.user_rating && obj.user_rating !== 0 && "Avgerage User Rating: "}
                                    {obj.user_rating && obj.user_rating !== 0 && obj.user_rating.toString() + "%"}
                                    {obj.user_rating && <br/>}

                                    {obj.safety_rating && obj.safety_rating !== 0 && "Avgerage Safety Rating: "}
                                    {obj.safety_rating && obj.safety_rating !== 0 && obj.safety_rating.toString()}
                                    {obj.safety_rating && obj.safety_rating !== 0 && <Rating name="half-rating-read" defaultValue={obj.safety_rating} precision={0.01} readOnly />}
                                    {obj.safety_rating && <br/>}
                                    
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                        <CardActions style={{position: "absolute", bottom: 0, right: 45}}>
                            {obj.website ? <Button color="primary" size="large" href={obj.website}>Website</Button> : ""}
                            {obj.twitter_handle ? <Button color="primary" size="large" href={"https://twitter.com/" + obj.twitter_handle}><FaTwitter /></Button> : ""}
                        </CardActions>
                    </Card>
                    <br/>
                </div>}

                {this.state.tableType === "providers" && <div style={{width: "300px", height: "440px", margin:"4px"}}>
                    <Card style={{height: "100%"}}>
                        <Link to={location => `${location.pathname}/${this.state.entryData.id}`}>
                            <CardActionArea style={{textAlign: "center"}}>
                                <img
                                src={obj.logo}
                                style={{width: "60%", height: "100%"}}
                                />
                                <CardContent>
                                <Typography gutterBottom variant="h6" component="h2">
                                    {obj.name}
                                </Typography>
                                <Typography gutterBottom variant="body2" component="p">
                                    {obj.location}
                                </Typography>
                                <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                    <br/>
                                    {obj.year_founded && "Est. "}
                                    {obj.year_founded && obj.year_founded.toString()}
                                    {obj.year_founded && <br/>}

                                    {obj.location_count && "Number of Locations: "}
                                    {obj.location_count && obj.location_count.toString()}
                                    {obj.location_count && <br/>}
                                    
                                    {obj.employee_count && "Number of Employees: "}
                                    {obj.employee_count && obj.employee_count.toString()}
                                    {obj.employee_count && <br/>}

                                    {obj.test_count && "Number of Crash Tests: "}
                                    {obj.test_count && obj.test_count.toString()}
                                    {obj.test_count && <br/>}
                                </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Link>
                        <CardActions style={{position: "absolute", bottom: 0, right: 70}}>
                            {obj.website ? <Button color="primary" size="large"l href={obj.website}>Website</Button> : ""}
                            {obj.twitter_handle ? <Button color="primary" size="large" href={"https://twitter.com/" + obj.twitter_handle}><FaTwitter /></Button> : ""}
                        </CardActions>
                    </Card>
                    <br/>
                </div>}

                {this.state.tableType === "models" && <div style={{width: "100%", height: "100%", marginLeft: "5px", marginRight: "5px"}}>
                    <Card style={{marginTop: "40px", height: "100%"}}>
                        <img
                        src={obj.img}
                        style={{width: "100%", height: "100%"}}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h6" component="h2">
                                <Link to={location => `${location.pathname}/${this.state.entryData.id}`}>
                                    <b>{obj.year}{' '}{obj.make}{' '}{obj.model}</b>
                                </Link>
                            </Typography>
                            <Typography gutterBottom variant="body2" component="p">
                                {obj.vehicle_type}
                            </Typography>
                            <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                <br/>
                                {obj.engine_type && "Engine: "}
                                {obj.engine_type && obj.engine_type}
                                {obj.engine_type && <br/>}

                                {obj.horse_power && "HP: "}
                                {obj.horse_power && obj.horse_power}
                                {obj.horse_power && <br/>}
                                
                                {(obj.IIHS_frontModerateOverlap != "N/A" && obj.IIHS_frontModerateOverlap) || (obj.IIHS_frontSmallOverlap != "N/A" && obj.IIHS_frontSmallOverlap) || (obj.IIHS_side != "N/A" && obj.IIHS_side) || (obj.IIHS_rollover != "N/A" && obj.IIHS_rollover) || (obj.IIHS_rear != "N/A" && obj.IIHS_rear) ? <h5><br /><b>IIHS Ratings</b></h5> : ""}
                                {obj.IIHS_frontModerateOverlap && obj.IIHS_frontModerateOverlap != "N/A" ? <h6>Front Moderate Overlap: {obj.IIHS_frontModerateOverlap}</h6> : ""}
                                {obj.IIHS_frontSmallOverlap && obj.IIHS_frontSmallOverlap != "N/A" ? <h6>Front Small Overlap: {obj.IIHS_frontSmallOverlap}</h6> : ""}
                                {obj.IIHS_side && obj.IIHS_side != "N/A" ? <h6>Side: {obj.IIHS_side}</h6> : ""}
                                {obj.IIHS_rollover && obj.IIHS_rollover != "N/A" ? <h6>Rollover: {obj.IIHS_rollover}</h6> : ""}
                                {obj.IIHS_rear && obj.IIHS_rear != "N/A" ? <h6>Rear: {obj.IIHS_rear}</h6> : ""}
                                <br/><br/>
                                {obj.NHTSA_overall_rating || obj.NHTSA_overall_side_crash_rating || obj.NHTSA_overall_front_crash_rating || obj.NHTSA_rollover_rating ? <h5><b>NHTSA Ratings</b></h5> : ""}
                                {obj.NHTSA_overall_rating && obj.NHTSA_overall_rating != 0 ? <h6>Overall: <Rating name="read-only" value={obj.NHTSA_overall_rating} readOnly /></h6> : ""}
                                {obj.NHTSA_overall_side_crash_rating && obj.NHTSA_overall_side_crash_rating != 0 ? <h6>Overall Side Crash: <Rating name="read-only" value={obj.NHTSA_overall_side_crash_rating} readOnly /></h6> : ""}
                                {obj.NHTSA_overall_front_crash_rating && obj.NHTSA_overall_front_crash_rating != 0 ? <h6>Overall Front Crash: <Rating name="read-only" value={obj.NHTSA_overall_front_crash_rating} readOnly /></h6> : ""}
                                {obj.NHTSA_rollover_rating && obj.NHTSA_rollover_rating != 0 ? <h6>Rollover: <Rating name="read-only" value={obj.NHTSA_rollover_rating} readOnly /></h6> : ""}
                                <br/>

                            </Typography>
                        </CardContent>
                    </Card>
                    <br/>
                </div>}

                {this.state.tableType === "tests" && <div style={{width: "100%", height: "100%", marginLeft: "5px", marginRight: "5px"}}>
                    <Card style={{marginTop: "40px", height: "100%"}}>
                        <CardContent>
                            <Typography gutterBottom variant="h3" color="textPrimary" component="h1">
                                <Link to={location => {
                                var idx = location.pathname.lastIndexOf("/");
                                var newPath = location.pathname.substring(0, idx);
                                idx = location.pathname.lastIndexOf("/");
                                newPath = newPath.substring(0, idx);
                                location.pathname = newPath;
                                return `/crashtest/${obj.id}`
                                }}>
                                    <b>#{obj.num}</b>
                                </Link>
                            </Typography>
                            <Typography gutterBottom variant="h6" color="textPrimary" component="h6">
                                {obj.test_performer ? "Performed by: " : ""}
                                {obj.test_performer ?
                                    <Link to={location => {
                                    var idx = location.pathname.lastIndexOf("/");
                                    var newPath = location.pathname.substring(0, idx);
                                    idx = location.pathname.lastIndexOf("/");
                                    newPath = newPath.substring(0, idx);
                                    location.pathname = newPath;
                                    return `/provider/${obj.performer_id}`
                                    }}>{obj.test_performer}</Link> : ""}
                            </Typography>
                            <Typography gutterBottom variant="body2" color="textPrimary" component="p">
                                <br/>
                                <b>{obj.test_type}</b>
                                <br/><br/>
                            </Typography>
                            <Typography gutterBottom variant="h6" color="textSecondary" component="h6">
                                {obj.model_id_1 ? "Vehicle 1: " : ""}
                                {obj.model_id_1 ? <Link to={location => {
                                    var idx = location.pathname.lastIndexOf("/");
                                    var newPath = location.pathname.substring(0, idx);
                                    idx = location.pathname.lastIndexOf("/");
                                    newPath = newPath.substring(0, idx);
                                    location.pathname = newPath;
                                    return `/carmodel/${obj.model_id_1}`
                                    }}>{obj.car_1_year} {obj.car_1_make} {obj.car_1_model}</Link> : ""}
                                {obj.model_id_2 ? "Vehicle 2: " : ""}
                                {obj.model_id_2 ? <Link to={location => {
                                    var idx = location.pathname.lastIndexOf("/");
                                    var newPath = location.pathname.substring(0, idx);
                                    idx = location.pathname.lastIndexOf("/");
                                    newPath = newPath.substring(0, idx);
                                    location.pathname = newPath;
                                    return `/carmodel/${obj.model_id_2}`
                                    }}>{obj.car_2_year} {obj.car_2_make} {obj.car_2_model}</Link> : ""}
                            </Typography>
                            <Typography style={{textAlign: "left"}} variant="body2" color="textSecondary" component="p">
                                <br/>
                                <h5>Variables {'&'} Results</h5>
                                {obj.impact_angle ? <h6>Impact Angle: {obj.impact_angle}&deg;</h6> : <h6>Impact Angle: 0&deg;</h6>}
                                {obj.model_id_1 ? <h6>Vehicle 1 Speed: {obj.collision_speed} km/h</h6> : ""}
                                {obj.model_id_2 ? <h6>Vehicle 2 Speed: {obj.collision_speed_2} km/h</h6> : ""}
                                {obj.model_id_1 ? <h6>Vehicle 1 Max Crush: {obj.max_crush} mm</h6> : ""}
                                {obj.model_id_2 ? <h6>Vehicle 2 Max Crush: {obj.max_crush_2} mm</h6> : ""}

                            </Typography>
                        </CardContent>
                    </Card>
                    <br/>
                </div>}
            </Col>
        );
    }
}

export default CompareItem;