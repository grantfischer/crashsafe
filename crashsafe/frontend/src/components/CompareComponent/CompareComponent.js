import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Typography from '@material-ui/core/Typography';
import { blue } from '@material-ui/core/colors';

import {
    Col,
    Row,
    ListGroup,
    ListGroupItem,
    Table,
    Spinner,
  } from 'reactstrap';

import CompareItem from './CompareItem'

class CompareComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            table: props.table,
        }
    }

    componentDidMount() {

    }

    render() {
        return(
            <Row>
                {this.state.data.map((item, i) => {return (
                <Col>
                    <CompareItem tableType={this.state.table} entryData={item} />
                </Col>)})}
            </Row>
        );
    }
}

export default CompareComponent;