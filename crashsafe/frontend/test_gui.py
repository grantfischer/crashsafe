import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

class TestGUI(unittest.TestCase) :

    def setUp(self) :
        # for docker
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(10)
        self.driver.get("http://crashsafe.me/")
    
    def test_title(self) :
        home_page_title = "CrashSafe"
        self.assertEqual(home_page_title, self.driver.title)
    
    def test_navbar(self):
        navbarLinks = self.driver.find_elements_by_class_name("nav-link")
        self.assertEqual(len(navbarLinks), 7)
    
    def test_car_brands(self) :
        self.driver.get("https://crashsafe.me/carbrand")
        cards = self.driver.find_elements_by_css_selector(".MuiPaper-root.MuiCard-root.MuiPaper-elevation1.MuiPaper-rounded")
        self.assertEqual(len(cards), 5)
    
    def test_car_models(self) :
        self.driver.get("https://crashsafe.me/carmodel")
        tableTitle = self.driver.find_element_by_id("tableTitle")
        self.assertEqual(tableTitle.text, "Car Models")

    def test_crash_tests(self) :
        self.driver.get("https://crashsafe.me/crashtest")
        tableTitle = self.driver.find_element_by_id("tableTitle")
        self.assertEqual(tableTitle.text, "Crash Tests")

    def test_providers(self) :
        self.driver.get("https://crashsafe.me/provider")
        cards = self.driver.find_elements_by_css_selector(".MuiPaper-root.MuiCard-root.MuiPaper-elevation1.MuiPaper-rounded")
        self.assertEqual(len(cards), 5)
    
    def test_about_page(self) :
        self.driver.get("https://crashsafe.me/about")
        try :
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "card"))
            )
            cards = self.driver.find_elements_by_class_name("card")
            self.assertEqual(len(cards), 9)
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def test_home_button(self) :
        self.driver.get("https://crashsafe.me/crashtest/6056")
        home_button = self.driver.find_elements_by_class_name("nav-link")[0]
        home_button.click()
        try :
            pageTitle = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, ".sc-fznOgF.bHrFnZ"))
            )
            self.assertEqual(pageTitle.text, "CrashSafe")
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def test_brand_instance_page(self) :
        self.driver.get("https://crashsafe.me/carbrand/563")
        try :
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "col-md-4"))
            )
            col4s = self.driver.find_elements_by_class_name("col-md-4")
            self.assertEqual(col4s[0].text, "ISUZU\nEst. 1916")
            self.assertEqual(len(col4s), 3)
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def test_model_instance_page(self) :
        self.driver.get("https://crashsafe.me/carmodel/2067")
        try :
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "col-md-6"))
            )
            col6s = self.driver.find_elements_by_class_name("col-md-6")
            self.assertEqual(len(col6s), 4)
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def test_crashtest_instance_page(self) :
        self.driver.get("https://crashsafe.me/crashtest/8094")
        try :
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "col-md-6"))
            )
            col6s = self.driver.find_elements_by_class_name("col-md-6")
            self.assertEqual(len(col6s), 2)
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def test_tester_instance_page(self) :
        self.driver.get("https://crashsafe.me/provider/4")
        try :
            WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "col-md-6"))
            )
            col6s = self.driver.find_elements_by_class_name("col-md-6")
            self.assertEqual(len(col6s), 2)
        except TimeoutException:
            print("Timeout Exception.")
            self.assertTrue(False)
    
    def tearDown(self) :
        self.driver.close()

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
