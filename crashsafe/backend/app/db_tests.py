# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import os
import db_utils
import sqlalchemy as db

from unittest import main, TestCase


class TestDB(TestCase):
    def test_can_connect(self):
        try:
            engine = db_utils.get_engine(verbose=False)
            engine.connect()
        except Exception as e:
            worked = False
            print("Caught exception:", e)
        else:
            worked = True
        self.assertTrue(worked)

    def test_get_table(self):
        table = db_utils.get_table("model")
        self.assertTrue(table is not None)
        self.assertTrue(isinstance(table, db.Table))

    def test_get_table_columns_by_string(self):
        columns = db_utils.get_table_column_titles("model")
        self.assertTrue(columns is not None)
        self.assertTrue(len(columns) > 0)
        self.assertEqual(columns[0:4], ['id', 'year', 'make', 'model'])

    def test_get_table_columns_by_table(self):
        table = db_utils.get_table("model")
        columns = db_utils.get_table_column_titles(table)
        self.assertTrue(columns is not None)
        self.assertTrue(len(columns) > 0)
        self.assertEqual(columns[0:4], ['id', 'year', 'make', 'model'])

    def test_get_rows_by_string(self):
        rows = db_utils.get_table_rows("model")
        self.assertTrue(rows is not None)
        self.assertTrue(len(rows) > 0)
        row = [row for row in rows if row[0] == 3]
        self.assertTrue(len(row) == 1) # should only have one row with id == 3
        row = list(row[0])
        self.assertEqual(row[0:4], [3, 1982, 'MERCURY', 'ZEPHYR'])

    def test_get_rows_by_table(self):
        table = db_utils.get_table("model")
        rows = db_utils.get_table_rows(table)
        self.assertTrue(rows is not None)
        self.assertTrue(len(rows) > 0)
        row = [row for row in rows if row[0] == 3]
        self.assertTrue(len(row) == 1) # should only have one row with id == 3
        row = list(row[0])
        self.assertEqual(row[0:4], [3, 1982, 'MERCURY', 'ZEPHYR'])

if __name__ == "__main__":  # pragma: no cover
    main()
