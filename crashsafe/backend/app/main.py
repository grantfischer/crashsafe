from flask import Flask, escape, request, jsonify
from flask_cors import CORS, cross_origin
import db_utils
from db_utils import get_table_column_titles, get_table
import sqlalchemy as db
import json

import faulthandler
faulthandler.enable()

app = Flask(__name__)
CORS(app)

engine = db_utils.get_engine(verbose=False)
connection = engine.connect()
metadata = db.MetaData()

models = db.Table("model", metadata, autoload=True, autoload_with=engine)
brands = db.Table("brand", metadata, autoload=True, autoload_with=engine)
tests = db.Table("test", metadata, autoload=True, autoload_with=engine)
providers = db.Table("tester", metadata, autoload=True, autoload_with=engine)

def queryToSet(query):
    resultProxy = connection.execute(query)
    return resultProxy.fetchall()

def singleResponse(query, table):
    resultSet = queryToSet(query)
    cols = get_table_column_titles(get_table(table))
    dictionaryObj = {cols[i]: resultSet[0][i] for i in range(len(cols))}
    jsonObj = json.dumps(dictionaryObj)
    return app.response_class(
        response=jsonObj, status=200, mimetype="application/json")

def getOneByID(table, num):
    query = db.select([table]).where(table.columns.id == int(num))
    return singleResponse(query, table)

def getOne(table):
    num = request.args.get("id", default=None)
    if num is not None:
        return getOneByID(table, num)
    name = request.args.get("name", default=None)
    if name is not None:
        query = db.select([table]).where(table.columns.name == name)
        return singleResponse(query, table)
    return "Bad Request\n"

def arrayResponse(query, table, limit=None):
    resultSet = queryToSet(query)
    if limit is None:
        limit = len(resultSet)
    else:
        limit = min(int(limit), len(resultSet))
    cols = get_table_column_titles(get_table(table))
    dictionaryObj = [{cols[i]: resultSet[j][i] for i in range(len(cols))}
        for j in range(limit)]
    jsonObj = json.dumps(dictionaryObj)
    return app.response_class(
        response=jsonObj, status=200, mimetype="application/json")

def listTable(table):
    limit = request.args.get("limit", default=None)
    return arrayResponse(db.select([table]), table, limit)

@app.route("/brand")
def getBrand():
    return getOne(brands)

@app.route("/model")
def getModel():
    num = request.args.get("id", default=None)
    if num is not None:
        return getOneByID(models, num)
    name = request.args.get("name", default=None)
    if name is not None:
        parts = name.split()
        query = db.select([models]).where(db.and_(models.columns.year == int(parts[0]), 
            models.columns.make == str(parts[1]),
            models.columns.model == str(parts[2])))
        return singleResponse(query, models)
    return "Bad Request\n"


@app.route("/test")
def getTest():
    num = request.args.get("id", default=None)
    if num is not None:
        return getOneByID(tests, num)
    return "Bad Request\n"

@app.route("/provider")
def getProvider():
    return getOne(providers)

def getSearchTerms(search):
    term_list = search.split()
    num_list = []
    for term in term_list:
        try:
            num = int(term)
            num_list.append(num)
        except Exception:
            pass
    return (term_list, num_list)

@app.route("/brands")
def listBrands():
    search = request.args.get("query", default=None)
    if search is not None:
        term_list, num_list = getSearchTerms(search)
        query = db.select([brands]).where(db.or_(brands.columns.year_founded.in_(num_list),
            *[brands.columns.name.ilike("%" + term + "%") for term in term_list],
            *[brands.columns.twitter_handle.ilike("%" + term + "%") for term in term_list]))
        return arrayResponse(query, brands)
    return listTable(brands)


@app.route("/models")
def listModels():
    brand = request.args.get("brand", default=None)
    if brand is not None:
        query = db.select([models]).where(models.columns.make == brand)
        return arrayResponse(query, models)
    provider = request.args.get("provider", default=None)
    if provider is not None:
        query = db.select([tests]).where(tests.columns.test_performer == provider)
        resultSet = queryToSet(query)
        model_ids = set()
        for test in resultSet:
            if test[3] is not None:
                model_ids.add(int(test[3]))
            if test[4] is not None:
                model_ids.add(int(test[4]))
        query = db.select([models]).where(models.columns.id.in_(model_ids))
        return arrayResponse(query, models)
    search = request.args.get("query", default=None)
    if search is not None:
        term_list, num_list = getSearchTerms(search)
        query = db.select([models]).where(db.or_(models.columns.year.in_(num_list), 
            *[models.columns.make.ilike("%" + term + "%") for term in term_list], 
            *[models.columns.model.ilike("%" + term + "%") for term in term_list], 
            *[models.columns.engine_type.ilike("%" + term + "%") for term in term_list], 
            *[models.columns.horse_power.ilike("%" + term + "%") for term in term_list], 
            *[models.columns.vehicle_type.ilike("%" + term + "%") for term in term_list]))
        resultSet = queryToSet(query)
        return arrayResponse(query, models)
    return listTable(models)


@app.route("/tests")
def listTests():
    model = request.args.get("model", default=None)
    if model is not None:
        query = db.select([tests]).where(db.or_(tests.columns.model_id_1 == int(model),
            tests.columns.model_id_2 == int(model)))
        return arrayResponse(query, tests)
    provider = request.args.get("provider", default=None)
    if provider is not None:
        query = db.select([tests]).where(tests.columns.test_performer == provider)
        return arrayResponse(query, tests)
    search = request.args.get("query", default=None)
    if search is not None:
        term_list, num_list = getSearchTerms(search)
        query = db.select([tests]).where(db.or_(tests.columns.num.in_(num_list),
            *[tests.columns.test_type.ilike("%" + term + "%") for term in term_list],
            *[tests.columns.test_performer.ilike("%" + term + "%") for term in term_list],
            tests.columns.car_1_year.in_(num_list),
            tests.columns.car_2_year.in_(num_list),
            *[tests.columns.car_1_make.ilike("%" + term + "%") for term in term_list],
            *[tests.columns.car_2_make.ilike("%" + term + "%") for term in term_list],
            *[tests.columns.car_1_model.ilike("%" + term + "%") for term in term_list],
            *[tests.columns.car_2_model.ilike("%" + term + "%") for term in term_list]))
        return arrayResponse(query, tests)
    return listTable(tests)


@app.route("/providers")
def listProviders():
    brand = request.args.get("brand", default=None)
    if brand is not None:
        query = db.select([models]).where(models.columns.make == brand)
        resultSet = queryToSet(query)
        model_ids = [resultSet[i][0] for i in range(len(resultSet))]
        query = db.select([tests]).where(db.or_(tests.columns.model_id_1.in_(model_ids),
                tests.columns.model_id_2.in_(model_ids)))
        resultSet = queryToSet(query)
        provs = [resultSet[i][5] for i in range(len(resultSet))]
        provider_names = set(provs)
        query = db.select([providers]).where(providers.columns.name.in_(provider_names))
        return arrayResponse(query, providers)
    search = request.args.get("query", default=None)
    if search is not None:
        term_list, num_list = getSearchTerms(search)
        query = db.select([providers]).where(db.or_(providers.columns.year_founded.in_(num_list),
            *[providers.columns.name.ilike("%" + term + "%") for term in term_list],
            *[providers.columns.location.ilike("%" + term + "%") for term in term_list],
            *[providers.columns.twitter_handle.ilike("%" + term + "%") for term in term_list]))
        return arrayResponse(query, providers)
    return listTable(providers)


@app.route("/")
def test():
    return "This is the CrashSafe API. See our Postman documentation at https://documenter.getpostman.com/view/10416711/SzKYNw2g?version=latest\n"


if __name__ == "__main__": 
    app.run(host ='0.0.0.0', port = 5000, debug = True)  
