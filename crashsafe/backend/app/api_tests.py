import unittest
import main as api


class Tests(unittest.TestCase):
    def setUp(self):
        self.app = api.app.test_client()

    def test_simple(self):
        self.assertTrue("crashsafe" in self.app.get("/").get_data(as_text=True).lower())

    def test_model(self):
        self.assertEqual(self.app.get("/model").get_data(as_text=True), "Bad Request\n")
        self.assertEqual(int(self.app.get("/model?id=4").json["year"]), 2011)
        self.assertEqual(
            int(
                self.app.get("/model?name=2011%20VOLVO%20XC60").json[
                    "NHTSA_overall_rating"
                ]
            ),
            5,
        )

    def test_brand(self):
        self.assertEqual(self.app.get("/brand").get_data(as_text=True), "Bad Request\n")
        self.assertEqual(self.app.get("/brand?id=448").json["name"], "ACURA")
        self.assertEqual(int(self.app.get("brand?name=ACURA").json["model_count"]), 48)

    def test_test(self):
        self.assertEqual(self.app.get("/test").get_data(as_text=True), "Bad Request\n")
        self.assertEqual(int(self.app.get("/test?id=1").json["model_id_1"]), 750)

    def test_provider(self):
        self.assertEqual(
            self.app.get("/provider").get_data(as_text=True), "Bad Request\n"
        )
        self.assertEqual(int(self.app.get("/provider?id=3").json["year_founded"]), 1943)
        self.assertEqual(
            self.app.get("provider?name=ENSCO").json["location"], "Springfield, VA"
        )

    def test_models(self):
        response = self.app.get("/models?limit=5").json
        self.assertEqual(len(response), 5)

    def test_providers(self):
        response = self.app.get("/providers").json
        self.assertEqual(len(response), 31)

    def test_brands(self):
        response = self.app.get("/brands?limit=5").json
        self.assertEqual(len(response), 5)

    def test_tests(self):
        response = self.app.get("/tests?limit=5").json
        self.assertEqual(len(response), 5)

    def test_model_by_id(self):
        response = self.app.get("/model?id=5").json
        self.assertEqual(response['make'], 'DODGE')

    def test_brand_by_id(self):
        response = self.app.get("/brand?id=460").json
        self.assertEqual(response['name'], 'AUDI')

    def test_test_by_id(self):
        response = self.app.get("/test?id=1").json
        self.assertEqual(response['num'], 6766)

    def test_provider_by_id(self):
        response = self.app.get("/provider?id=7").json
        self.assertEqual(response['name'], 'EXPONENT')

    def test_list_models_by_brand(self):
        response = self.app.get("/models?brand=DAIHATSU").json
        self.assertEqual(len(response), 2)

if __name__ == "__main__":
    unittest.main()
