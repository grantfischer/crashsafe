"""
Internal utility functions for interacting with the database.
"""

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import os
import sqlalchemy as db

from dotenv import load_dotenv


def get_engine(verbose=False):
    """Get SQL engine object for CrasSafe database.

    Notes:
      - Does not connect to the engine. engine.connect() still needs to be
        called on the returned object.
    
    Args:
      verbose (bool): print debugging information.

    Returns:
      SQLAlchemy engine object for the database specified the env variables.
    """

    # If there is a .env file in the same dir as this module, load it
    # otherwise we just assume the user has already set the env variables
    # like in the case this code runs on GitLab CI
    dotenv_filepath = os.path.join(os.path.dirname(__file__), "../.env")
    if os.path.isfile(dotenv_filepath):
        load_dotenv()

    DB_NAME = os.getenv("DB_NAME")
    DB_HOST = os.getenv("DB_HOST")
    DB_PORT = os.getenv("DB_PORT")
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")

    url = "mysql+mysqldb://{}:{}@{}:{}/{}".format(
        DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME
    )
    if verbose:
        print("Connecting to :", url)

    engine = db.create_engine(url)

    return engine


def get_table(table):
    """Get table from database."""
    return db.Table(table, METADATA, autoload=True, autoload_with=ENGINE)


def get_table_column_titles(table):
    """Get column tites of tables.

    Args:
      table (string or db.Table): Table to get columns of. If table is a string

    Returns:
      list of strings of column names.
    """
    if isinstance(table, str):
        table = get_table(table)

    return table.columns.keys()


def get_table_rows(table):
    """Get all rows in table.

    Args:
      table (string or db.Table): Table to get columns of. If table is a string

    Returns:
      Rows of table
    """
    if isinstance(table, str):
        table = get_table(table)

    query = db.select([table])
    result_proxy = CONNECTION.execute(query)
    return result_proxy.fetchall()


def set_model_value(make, model, year, column, value):
    """Update a value in the model table.
    
    Args:
      make (string)
      model (string)
      year (int)
      column (string): Title of column to update
      value: Value to put in entry for column
    
    Raise:
      ValueError if row for make, model, year cannot be found
    """
    try:
        query = (
            db.update(MODEL_TABLE)
            .values(column=value)
            .where(make=make)
            .where(year=year)
            .where(model=model)
        )
        result_proxy = CONNECTION.execute(query)
    except Exception as e:
        raise ValueError(
            "Error setting '{}'={} for row matching 'make'={}, 'model'={}, 'year'={}".format(
                column, value, make, model, year
            )
        )


# Define these global variables on import
ENGINE = get_engine(verbose=True)
CONNECTION = ENGINE.connect()
METADATA = db.MetaData()

MODEL_TABLE = get_table("model")
