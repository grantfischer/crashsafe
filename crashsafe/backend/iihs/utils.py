import sys
sys.path.append("../")
import db_utils

import sqlalchemy as db

def get_table(table):
    """Get table from database."""
    return db.Table(table, METADATA, autoload=True, autoload_with=ENGINE)

def get_table_column_titles(table):
    """Get column tites of tables.

    Args:
      table (string or db.Table): Table to get columns of. If table is a string

    Returns:
      list of strings of column names.
    """
    if isinstance(table, str):
        table = get_table(table)

    return table.columns.keys()

def get_table_rows(table):
    """Get all rows in table.

    Args:
      table (string or db.Table): Table to get columns of. If table is a string

    Returns:
      Rows of table
    """
    if isinstance(table, str):
        table = get_table(table)

    query = db.select([table])
    result_proxy = CONNECTION.execute(query)
    return result_proxy.fetchall()

def set_model_value(make, model, year, column, value):
    """Update a value in the model table.
    
    Args:
      make (string)
      model (string)
      year (int)
      column (string): Title of column to update
      value: Value to put in entry for column
    
    Raise:
      ValueError if row for make, model, year cannot be found
    """
    try:
        val = {column: value}
        query = db.update(MODEL_TABLE).values(**val).where(
            MODEL_TABLE.columns.make==make).where(
            MODEL_TABLE.columns.year==year).where(
            MODEL_TABLE.columns.model==model)
        result_proxy = CONNECTION.execute(query)
    except Exception as e:
        raise ValueError("Error setting '{}'={} for row matching 'make'={}, 'model'={}, 'year'={}".format(
            column, value, make, model, year))

# Define these global variables on import
ENGINE = db_utils.get_engine(verbose=True)
CONNECTION = ENGINE.connect()
METADATA = db.MetaData()

MODEL_TABLE = get_table("model")