import sys
import os
import requests
from dotenv import load_dotenv

sys.path.append("../")
import db_utils

# If there is a .env file in the same dir as this module, load it
# otherwise we just assume the user has already set the env variables
# like in the case this code runs on GitLab CI
dotenv_filepath = "../.env"
if os.path.isfile(dotenv_filepath):
    load_dotenv()

IIHS_URL = 'https://api.iihs.org/v4'
IIHS_ACCOUNT = os.getenv('IIHS_ACCOUNT')
IIHS_KEY = os.getenv('IIHS_KEY')

OVERALL_RATINGS = ['frontalRatingsModerateOverlap',
                   'frontalRatingsSmallOverlap',
                   'sideRatings',
                   'rolloverRatings',
                   'rearRatings',
                   'headlightRatings']

def build_url(query):
    """Builds a working URL for a query to the IIHS database.

    Builds URL of the form:
      IIHS_URL/query?apikey=IIHS_KEY&format=json

    Args:
      query (string): Valid query to IIHS database. E.g. 
        "ratings/single/2020/acura/ilx-4-door-sedan"

    Returns:
      url to make API request with.
    """
    return '{}/{}?apikey={}&format=json'.format(
            IIHS_URL, query, IIHS_KEY)

def get_query(query):
    """Query the IIHS API.

    Args:
      query (string): Valid query to IIHS database. E.g. 
        "ratings/single/2020/acura/ilx-4-door-sedan"

    Returns:
      JSON object if query succeeds, else None
    """
    url = build_url(query)
    response = requests.get(url)
    if response.status_code == 200:
        r = response.json()
        if r == {} or r == []:
            return None
        return r
    return None

def get_all_series(year, make):
    """Get all series for a given make and year."""
    query = 'ratings/series/{}/{}'.format(year, make)
    return get_query(query)

def get_single_series(year, make, model):
    """Get ratings for a single make, model, and year."""
    slug = get_slug(year, make, model)
    query = 'ratings/single/{}/{}/{}'.format(year, make, slug)
    result = get_query(query)
    return result[0]

def get_slug(year, make, model):
    """Get first matching model for year and make.

    Search all slugs for a given make and year and find the first slug
    that contains `model`. This function is effectively how you find
    the correct model name to use in `get_single_series`.

    Example:
      The model ILX for a 2020 Acura is actually called "ilx-4-door-sedan"
      so this function will return "ilx-4-door-sedan"

    Returns:
        slug name (a.k.a model)
    """
    series = get_all_series(year, make)
    for s in series:
        if model.lower() in s['slug'].lower():
            return s['slug']
    return None

def get_overall_ratings(year, make, model):
    """Return dictionary of overall ratings for a model."""
    results = get_single_series(year, make, model)

    ratings = {}

    for rating in OVERALL_RATINGS:
        try:
            r = results[rating][0]
            r = r['overallRating']
        except:
            r = 'N/A'
        ratings['IIHS_' + rating] = r

    return ratings


if __name__ == __main__:
    m = get_single_series(2020, "acura", "ilx")
    print(m)
    r = get_overall_ratings(2020, "acura", "ilx")
    print(r)