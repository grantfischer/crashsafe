#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from unittest import main, TestCase
from io import StringIO

from Collatz import collatz_eval, collatz_run

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(420554, 825621)
        self.assertEqual(v, 509)

    def test_eval_6(self):
        v = collatz_eval(391341, 328468)
        self.assertEqual(v, 441)

    def test_eval_7(self):
        v = collatz_eval(148341, 529277)
        self.assertEqual(v, 470)

    def test_eval_8(self):
        v = collatz_eval(569023, 981098)
        self.assertEqual(v, 525)

    def test_eval_9(self):
        v = collatz_eval(243933, 44170)
        self.assertEqual(v, 443)

    def test_eval_10(self):
        v = collatz_eval(928490, 696126)
        self.assertEqual(v, 525)

    def test_run_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_run(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
